﻿using Dibk.Ftpb.Integration.SvarUt.Models;
using System;

namespace Dibk.Ftpb.Integration.SvarUt
{
    public class SvarUtRequestException : Exception
    {
        /// <summary>
        /// Statuscode from the actual HttpResponseMessage
        /// </summary>
        public System.Net.HttpStatusCode HttpStatusCode { get; set; }

        /// <summary>
        /// The error reported by SvarUt
        /// </summary>
        ///
        public SvarUtError SvarUtError { get; set; }

        public SvarUtRequestException(System.Net.HttpStatusCode httpStatusCode, SvarUtError svarUtError) : base(svarUtError.Message)
        {
            HttpStatusCode = httpStatusCode;
            SvarUtError = svarUtError;
        }

        /// <summary>
        /// String of the exception
        /// </summary>
        /// <returns>Pattern is: 'SvarUt request exception: {SvarUtErrorCode} - {SvarUtErrorMessage}  HttpsStatusCode: {HttpStatusCode}'</returns>
        public override string ToString()
        {
            return $"SvarUt request exception: {SvarUtError.Error} - {SvarUtError.Message}  HttpsStatusCode: {HttpStatusCode}";
        }
    }
}