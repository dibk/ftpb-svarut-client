﻿using Dibk.Ftpb.Integration.SvarUt.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Integration.SvarUt
{
    public interface ISvarUtClient
    {
        /// <summary>
        /// Sets authentication header for HttpRequest
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        void SetAuthentication(string userName, string password);

        /// <summary>
        /// Retrieves a collection of forsendelsestyper from SvarUt.
        /// Uri: /tjenester/api/forsendelse/v1/forsendelseTyper
        /// </summary>
        /// <returns cref="ForsendelseTyperResult">Returns what is found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        Task<ForsendelseTyperResult> GetForsendelsesTyperAsync();

        /// <summary>
        /// Retrieves a collection of forsendelsestyper from SvarUt.
        /// Uri: /tjenester/api/forsendelse/v1/forsendelseTyper
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns cref="ForsendelseTyperResult">Returns what is found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        Task<ForsendelseTyperResult> GetForsendelsesTyperAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Sends a forsendelse to SvarUt and returns the id.
        /// Uri: /tjenester/api/forsendelse/v1/sendForsendelse
        /// </summary>
        /// <param cref="Forsendelse" name="forsendelse">A complete forsendelse</param>
        /// <returns cref="ForsendelsesId">The SvarUt forsendelse id</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when constraints in the 'Forsendelse' object are broken</exception>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        Task<ForsendelsesId> SendForsendelseAsync(Forsendelse forsendelse);

        /// <summary>
        /// Sends a forsendelse to SvarUt and returns the id.
        /// Uri: /tjenester/api/forsendelse/v1/sendForsendelse
        /// </summary>
        /// <param cref="Forsendelse" name="forsendelse">A complete forsendelse</param>
        /// <param name="cancellationToken"></param>
        /// <returns cref="ForsendelsesId">The SvarUt forsendelse id</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when constraints in the 'Forsendelse' object are broken</exception>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        Task<ForsendelsesId> SendForsendelseAsync(Forsendelse forsendelse, CancellationToken cancellationToken);

        /// <summary>
        /// Sends a forsendelse to SvarUt and returns the id.
        /// Uri: /tjenester/api/forsendelse/v1/{id}/sendForsendelse
        /// </summary>
        /// <param name="forsendelseId">ForsendelseId created by using GetNyForsendelseId</param>
        /// <param cref="Forsendelse" name="forsendelse">A complete forsendelse</param>
        /// <returns cref="ForsendelsesId">The SvarUt forsendelse id</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when constraints in the 'Forsendelse' object are broken</exception>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        Task<ForsendelsesId> SendForsendelseAsync(string forsendelseId, Forsendelse forsendelse);

        /// <summary>
        /// Sends a forsendelse to SvarUt and returns the id.
        /// Uri: /tjenester/api/forsendelse/v1/{id}/sendForsendelse
        /// </summary>
        /// <param name="forsendelseId">ForsendelseId created by using GetNyForsendelseId</param>
        /// <param cref="Forsendelse" name="forsendelse">A complete forsendelse</param>
        /// <param name="cancellationToken"></param>
        /// <returns cref="ForsendelsesId">The SvarUt forsendelse id</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when constraints in the 'Forsendelse' object are broken</exception>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        Task<ForsendelsesId> SendForsendelseAsync(string forsendelseId, Forsendelse forsendelse, CancellationToken cancellationToken);

        /// <summary>
        /// Retrieves the status of a given forsendelse.
        /// Uri: /tjenester/api/forsendelse/v1/{forsendelseId}/status
        /// </summary>
        /// <param name="forsendelseId">The forsendelse </param>
        /// <returns cref="ForsendelseStatus">Status of the forsendelse. For 404 responses it returns null</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when constraints in the 'ForsendelseStatus' object are broken</exception>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        Task<ForsendelseStatus> GetForsendelseStatusAsync(string forsendelseId);

        /// <summary>
        /// Retrieves the status of a given forsendelse.
        /// Uri: /tjenester/api/forsendelse/v1/{forsendelseId}/status
        /// </summary>
        /// <param name="forsendelseId">The forsendelse </param>
        /// <param name="cancellationToken"></param>
        /// <returns cref="ForsendelseStatus">Status of the forsendelse. For 404 responses it returns null</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when constraints in the 'ForsendelseStatus' object are broken</exception>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        Task<ForsendelseStatus> GetForsendelseStatusAsync(string forsendelseId, CancellationToken cancellationToken);

        /// <summary>
        /// Get forsendelses historikk for a given id.
        /// Uri: /tjenester/api/forsendelse/v1/{forsendelseId}/historikk
        /// </summary>
        /// <param name="forsendelseId"></param>
        /// <returns cref="ForsendelseHistorikk">Returns what is found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        Task<ForsendelseHistorikk> GetForsendelseHistorikkAsync(string forsendelseId);

        /// <summary>
        /// Get forsendelses historikk for a given id.
        /// Uri: /tjenester/api/forsendelse/v1/{forsendelseId}/historikk
        /// </summary>
        /// <param name="forsendelseId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns cref="ForsendelseHistorikk">Returns what is found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        Task<ForsendelseHistorikk> GetForsendelseHistorikkAsync(string forsendelseId, CancellationToken cancellationToken);

        /// <summary>
        /// Retrieves forsendelsestyper for a given organizational number.
        /// Uri: /tjenester/api/forsendelse/v1/mottakersystem/{organisasjonsnummer}
        /// </summary>
        /// <param name="organisasjonsnummer"></param>
        /// <returns cref="MottakerForsendelseTyperResult">Returns what is found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        Task<MottakerForsendelseTyperResult> GetMottakerForsendelseTyperAsync(string organisasjonsnummer);

        /// <summary>
        /// Retrieves forsendelsestyper for a given organizational number.
        /// </summary>
        /// <param name="organisasjonsnummer"></param>
        /// <param name="cancellationToken"></param>
        /// <returns cref="MottakerForsendelseTyperResult">Returns what is found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        Task<MottakerForsendelseTyperResult> GetMottakerForsendelseTyperAsync(string organisasjonsnummer, CancellationToken cancellationToken);

        /// <summary>
        /// Retreives a list of information about documents which are a part of a 'forsendelse'.
        /// Uri: /tjenester/api/forsendelse/v1/{forsendelseId}/dokumentmetadata
        /// </summary>
        /// <param name="forsendelseId"></param>
        /// <returns cref="DokumentMetadataResult">Returns what is found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        Task<DokumentMetadataResult> GetDokumentMetadataAsync(string forsendelseId);

        /// <summary>
        /// Retreives a list of information about documents which are a part of a 'forsendelse'.
        /// Uri: /tjenester/api/forsendelse/v1/{forsendelseId}/dokumentmetadata
        /// </summary>
        /// <param name="forsendelseId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns cref="DokumentMetadataResult">Returns what is found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        Task<DokumentMetadataResult> GetDokumentMetadataAsync(string forsendelseId, CancellationToken cancellationToken);

        /// <summary>
        /// Retrieves statuses for a range of forsendelsesIds.
        /// Uri: /tjenester/api/forsendelse/v1/statuser
        /// </summary>
        /// <param name="forsendelseIder">Range of forsendelseIds</param>
        /// <returns cref="ForsendelseStatusResult">Returns the result if found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        Task<ForsendelseStatusResult> GetForsendelseStatuserAsync(List<string> forsendelseIder);

        /// <summary>
        /// Retrieves statuses for a range of forsendelsesIds.
        /// Uri: /tjenester/api/forsendelse/v1/statuser
        /// </summary>
        /// <param name="forsendelseIder">Range of forsendelseIds</param>
        /// <param name="cancellationToken"></param>
        /// <returns cref="ForsendelseStatusResult">Returns the result if found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        Task<ForsendelseStatusResult> GetForsendelseStatuserAsync(List<string> forsendelseIder, CancellationToken cancellationToken);

        /// <summary>
        /// Creates a new ForsendelseId in SvarUt.
        /// Uri: /tjenester/api/forsendelse/v1/startNyForsendelse
        /// </summary>
        /// <returns cref="ForsendelsesId">A new ForsendelseId. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        Task<ForsendelsesId> GetNyForsendelseIdAsync();

        /// <summary>
        /// Creates a new ForsendelseId in SvarUt.
        /// Uri: /tjenester/api/forsendelse/v1/startNyForsendelse
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns cref="ForsendelsesId">A new ForsendelseId. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        Task<ForsendelsesId> GetNyForsendelseIdAsync(CancellationToken cancellationToken);

        /*
        Task SettLest(string forsendelseId, LestAv lestAv);
        Task SettLest(string forsendelseId, LestAv lestAv, CancellationToken cancellationToken);

        Task<SigneringsHistorikk> GetSigneringsHistorikk(string forsendelseId);
        Task<SigneringsHistorikk> GetSigneringsHistorikk(string forsendelseId, CancellationToken cancellationToken);
        */
    }
}