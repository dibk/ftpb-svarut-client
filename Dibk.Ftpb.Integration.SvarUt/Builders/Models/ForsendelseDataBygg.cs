﻿using System.Collections.Generic;
using System.Text;

namespace Dibk.Ftpb.Integration.SvarUt.Builders.Models
{
    /// <summary>
    /// Modell for å bygge forsendelsedata for byggesøknader.
    /// </summary>
    public class ForsendelseDataBygg
    {
        /// <summary>
        /// Oppretter en ny instans av ForsendelseDataBygg med angitt arkivreferanse.
        /// </summary>
        /// <param name="archiveReference">Arkivreferanse for forsendelsen.</param>
        public ForsendelseDataBygg(string archiveReference)
        {
            ArchiveReference = archiveReference;
        }

        /// <summary>
        /// Henter arkivreferansen for forsendelsen.
        /// </summary>
        /// <example>6b984dd82669</example>
        public string ArchiveReference { get; }

        /// <summary>
        /// Henter eller setter adresselinje 1.
        /// </summary>
        /// <example>Storgata 1</example>
        public string Adresselinje1 { get; set; }

        /// <summary>
        /// Henter eller setter adresselinje 2.
        /// </summary>
        /// <example>c/o Ola Nordmann</example>
        public string Adresselinje2 { get; set; }

        /// <summary>
        /// Henter eller setter adresselinje 3.
        /// </summary>
        /// <example>Postboks 123</example>
        public string Adresselinje3 { get; set; }

        /// <summary>
        /// Henter eller setter avgivende system.
        /// </summary>
        /// <example>Byggesak</example>
        public string AvgivendeSystem { get; set; }

        /// <summary>
        /// Henter eller setter bolignummer.
        /// </summary>
        /// <example>1</example>
        public string Bolignummer { get; set; }

        /// <summary>
        /// Henter eller setter bruksnummer.
        /// </summary>
        /// <example>1</example>
        public string Bruksnummer { get; set; }

        /// <summary>
        /// Henter eller setter bygningsnummer.
        /// </summary>
        /// <example>1</example>
        public string Bygningsnummer { get; set; }

        /// <summary>
        /// Henter eller setter festenummer.
        /// </summary>
        /// <example>1</example>
        public string Festenummer { get; set; }

        /// <summary>
        /// Henter eller setter gårdsnummer.
        /// </summary>
        /// <example>1</example>
        public string Gårdsnummer { get; set; }

        /// <summary>
        /// Henter eller setter kommunens saksnummer år.
        /// </summary>
        /// <example>2021</example>
        public string KommunensSaksnummerÅr { get; set; }

        /// <summary>
        /// Henter eller setter kommunens saksnummer sekvensnummer.
        /// </summary>
        /// <example>123</example>
        public string KommunensSaksnummerSekvensnummer { get; set; }

        /// <summary>
        /// Henter eller setter kommunenummer.
        /// </summary>
        /// <example>1234</example>
        public string Kommunenummer { get; set; }

        /// <summary>
        /// Henter eller setter landkode.
        /// </summary>
        /// <example>NO</example>
        public string Landkode { get; set; }

        /// <summary>
        /// Henter eller setter postnummer.
        /// </summary>
        /// <example>1234</example>
        public string Postnr { get; set; }

        /// <summary>
        /// Henter eller setter poststed.
        /// </summary>
        /// <example>Oslo</example>
        public string Poststed { get; set; }

        /// <summary>
        /// Henter eller setter seksjonsnummer.
        /// </summary>
        /// <example>1</example>
        public string Seksjonsnummer { get; set; }

        /// <summary>
        /// Henter eller setter navn på søknadsskjema.
        /// </summary>
        /// <example>Byggesøknad</example>
        public string SøknadSkjemaNavn { get; set; }

        /// <summary>
        /// Henter eller setter tiltakstype.
        /// </summary>
        /// <example>Oppføring av nybygg</example>
        public string TiltakType { get; set; } = string.Empty;

        /// <summary>
        /// Henter eller setter forsendelsestype.
        /// </summary>
        /// <example>Byggesøknad</example>
        public string ForsendelseType { get; set; } = "Byggesøknad";

        /// <summary>
        /// Henter eller setter konteringskode.
        /// </summary>
        /// <example>1234</example>
        public string Konteringskode { get; set; }

        /// <summary>
        /// Til bruk for å tagge forsendelsestittel. Settes typisk ifm forskjellige miljø og ikke i produksjon
        /// </summary>
        public string ForsendelseTittelPrefix { get; set; }

        /// <summary>
        /// Til bruk for å tagge forsendelsestittel. Settes typisk ifm forskjellige miljø og ikke i produksjon
        /// </summary>
        public string ForsendelseTittelSuffix { get; set; }

        private string _forsendelseTittel;

        /// <summary>
        /// Henter eller setter forsendelsestittel.
        /// Dersom den ikke er satt, vil den bli generert fra et standard-tagmønster. {Adresselinje1} {SøknadSkjemaNavn} {Gårdsnummer}/{Bruksnummer}         ///
        /// </summary>
        /// <example>Byggevegen 52 Søknad om igangsettingstillatelse 1234/5678</example>
        public string ForsendelseTittel
        {
            get => BuildForsendelseTittel();
            set => _forsendelseTittel = value;
        }

        private string BuildForsendelseTittel()
        {
            string title = string.Empty;
            if (string.IsNullOrEmpty(_forsendelseTittel))
                title = MapFromTagPattern(fallbackTitlePattern);
            else
                title = _forsendelseTittel;

            if (!string.IsNullOrEmpty(ForsendelseTittelPrefix))
                title = ForsendelseTittelPrefix + " " + title;

            if (!string.IsNullOrEmpty(ForsendelseTittelSuffix))
                title = title + " " + ForsendelseTittelSuffix;

            return title;
        }

        private static string fallbackTitlePattern = $"{TemplateTags.Adresselinje1} {TemplateTags.SøknadSkjemaNavn} {TemplateTags.Gårdsnummer}/{TemplateTags.Bruksnummer}";

        /// <summary>
        /// Mapper data fra tagmønster til faktiske verdier.
        /// </summary>
        /// <param name="tagPattern">Tagmønster som skal mappes.</param>
        /// <returns>Mappet tekst basert på tagmønster.</returns>
        public string MapFromTagPattern(string tagPattern)
        {
            StringBuilder sb = new StringBuilder(tagPattern);

            // Erstatter taggene med faktiske verdier
            sb.Replace(TemplateTags.Gårdsnummer, Gårdsnummer);
            sb.Replace(TemplateTags.Bruksnummer, Bruksnummer);
            sb.Replace(TemplateTags.Festenummer, Festenummer);
            sb.Replace(TemplateTags.Seksjonsnummer, Seksjonsnummer);
            sb.Replace(TemplateTags.Adresselinje1, Adresselinje1);
            sb.Replace(TemplateTags.SøknadSkjemaNavn, SøknadSkjemaNavn);
            sb.Replace(TemplateTags.AltinnArkivReferanse, ArchiveReference);

            return sb.ToString();
        }

        /// <summary>
        /// Konfigurasjon for klassifisering.
        /// </summary>
        /// <example> (1, "GBNR", "{gnr}/{bnr}/{fnr}/{snr}"), (2, "SID", "{altinnreferanse}"), </example>
        public static List<(int sorting, string name, string value)> ClassificationConfig = new List<(int sorting, string name, string value)>()
            {
                (1, "GBNR", "{gnr}/{bnr}/{fnr}/{snr}"),
                (2, "SID", "{altinnreferanse}"),
            };

        internal static class TemplateTags
        {
            public static string Gårdsnummer = "{gnr}";
            public static string Bruksnummer = "{bnr}";
            public static string Festenummer = "{fnr}";
            public static string Seksjonsnummer = "{snr}";
            public static string AltinnArkivReferanse = "{altinnreferanse}";
            public static string Adresselinje1 = "{adr1}";
            public static string SøknadSkjemaNavn = "{skjema}";
            public static string Tiltak = "{tiltak}";
        }
    }
}