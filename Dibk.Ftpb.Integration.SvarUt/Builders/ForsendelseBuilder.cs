using Dibk.Ftpb.Integration.SvarUt.Builders.Models;
using Dibk.Ftpb.Integration.SvarUt.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Dibk.Ftpb.Integration.SvarUt.Builders
{
    /// <summary>
    /// Builder for å bygge en SvarUt forsendelse som passer for fellestjenester plan og bygg.
    /// </summary>
    public class ForsendelseBuilder
    {
        /// <summary>
        /// Lag en ny instans av ForsendelseBuilder.
        /// </summary>
        /// <param name="forsendelseData"></param>
        /// <param name="getConfig"></param>
        public ForsendelseBuilder(ForsendelseDataBygg forsendelseData, Func<string, (string, string, string, string, string)> getConfig)
        {
            _forsendelseData = forsendelseData;
            _getConfig = getConfig;
            _forsendelse = new SvarUtForsendelse()
            {
                AvgivendeSystem = forsendelseData.AvgivendeSystem,
                EksternReferanse = forsendelseData.ArchiveReference,
                Tittel = GetForsendelseTitle(),
                ForsendelsesType = forsendelseData.ForsendelseType,
                MetadataFraAvleverendeSystem = new NoarkMetadataFraAvleverendeSaksSystem()
                {
                    Tittel = GetForsendelseTitle(),
                },
                TemporaryDocumentStore = new List<SortableSvarUtDocument>(),
                KonteringsKode = forsendelseData.Konteringskode
            };

            AddMetadataForImport(forsendelseData.KommunensSaksnummerÅr, forsendelseData.KommunensSaksnummerSekvensnummer, DateTime.Now);
        }

        private readonly ForsendelseDataBygg _forsendelseData;
        private readonly Func<string, (string dataType, string displayName, string arkivlett1DokumentType, string arkivlett2DokumentType, string Category)> _getConfig;
        private readonly SvarUtForsendelse _forsendelse;

        private string GetForsendelseTitle()
        {
            return _forsendelseData.ForsendelseTittel;
        }

        /// <summary>
        /// Legg til grunnleggende klasseringsverdier
        /// </summary>
        public void AddBasicClassificationValues()
        {
            var entries = new List<Entry>();
            foreach (var classification in ForsendelseDataBygg.ClassificationConfig)
            {
                var classificationValue = _forsendelseData.MapFromTagPattern(classification.value);
                //Legacy style
                entries.Add(new Entry()
                {
                    Key = $"{classification.sorting}_{classification.name}",
                    Value = classificationValue
                });

                //Latest style
                entries.Add(new Entry()
                {
                    Key = $"klassering.{classification.sorting}.{classification.name}",
                    Value = classificationValue
                });
            }

            _forsendelse.MetadataFraAvleverendeSystem.EkstraMetadata = entries;
        }

        private void AddMetadataForImport(string saksår, string sakssekvensnummer, DateTime? forsendelseDato)
        {
            long.TryParse(saksår, out var convertedSaksår);
            long.TryParse(sakssekvensnummer, out var convertedSakssekvensnr);

            _forsendelse.MetadataForImport = new NoarkMetadataForImport()
            {
                SaksAar = convertedSaksår,
                SaksSekvensNummer = convertedSakssekvensnr,
                DokumentetsDato = forsendelseDato.HasValue ? forsendelseDato.Value : DateTime.Now,
                JournalPostType = "I",
                Tittel = GetForsendelseTitle()
            };
        }

        /// <summary>
        /// Legg til digital mottaker
        /// </summary>
        /// <param name="receiverName"></param>
        /// <param name="orgnr"></param>
        public void AddDigitalReceiver(string receiverName, string orgnr)
        {
            _forsendelse.Mottaker = new Adresse()
            {
                DigitalAdresse = new Digitaladresse()
                {
                    OrganisasjonsNummer = orgnr
                },
                PostAdresse = new PostAdresse()
                {
                    Navn = receiverName,
                    PostNummer = "9999",
                    PostSted = "Digital levering"
                }
            };
            _forsendelse.KunDigitalLevering = true;
        }

        /// <summary>
        /// Legg til mottaker av forsendelsen
        /// </summary>
        /// <param name="receiver"></param>
        public void AddReceiver(Adresse receiver)
        {
            _forsendelse.Mottaker = receiver;
        }

        /// <summary>
        /// Legg til avsender av forsendelsen
        /// </summary>
        /// <param name="sender"></param>
        public void AddSender(Adresse sender)
        {
            _forsendelse.SvarSendesTil = sender;
        }

        /// <summary>
        /// Bygger forsendelse
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">Kaster exception dersom enten mottaker eller avsender er tomme</exception>
        public Forsendelse Build()
        {
            if (_forsendelse.Mottaker == null)
                throw new ArgumentNullException("Forsendelse.Mottaker has to have a value");

            if (_forsendelse.SvarSendesTil == null)
                throw new ArgumentNullException("Forsendelse.SvarSendesTil has to have a value");

            //sorter dokument
            SortDocuments();
            _forsendelse.Dokumenter = _forsendelse.TemporaryDocumentStore.Select(p => p as IDokument).ToList();
            return _forsendelse;
        }

        private void SortDocuments()
        {
            if (_forsendelse.TemporaryDocumentStore != null)
            {
                _forsendelse.TemporaryDocumentStore = _forsendelse.TemporaryDocumentStore.OrderBy(a => a.SortingSection).ToList();

                for (int i = 0; i < _forsendelse.TemporaryDocumentStore.Count; i++)
                {
                    var fileNameIdx = i + 1;

                    _forsendelse.TemporaryDocumentStore[i].Filnavn = EnforceFilenameLength($"{fileNameIdx}_{_forsendelse.TemporaryDocumentStore[i].Filnavn}");
                    var skjemaversjonMetadata = _forsendelse.TemporaryDocumentStore[i].EkstraMetadata?.SingleOrDefault(p => p.Key.Equals("Skjemaversjon"));
                    if (skjemaversjonMetadata != null)
                        _forsendelse.MetadataFraAvleverendeSystem.EkstraMetadata.Add(new Entry()
                        {
                            Key = $"skjemaversjon.dokument.{fileNameIdx}", //Eksisterende logikk
                            Value = skjemaversjonMetadata.Value
                        });
                }
            }
        }

        /// <summary>
        /// Legg til hovedskjema
        /// </summary>
        /// <param name="dataType"></param>
        /// <param name="filename"></param>
        /// <param name="content"></param>
        /// <param name="dataModelVersion"></param>
        public void AddMainFormXml(string dataType, string filename, Stream content, string dataModelVersion, List<KeyValuePair<string, string>> ekstraMetadata = null)
        {
            var fileName = Path.GetFileNameWithoutExtension(filename);
            var fileExtension = Path.GetExtension(filename);
            var newFilename = $"{fileName}_KAN-INNEHOLDE-PERSONOPPLYSNINGER{fileExtension}";

            var doc = new SvarUtFormDocument(dataModelVersion,
                                                      SortingSection.MainXml,
                                                      content,
                                                      CreateValidFileName(newFilename),
                                                      _getConfig(dataType), 
                                                      ekstraMetadata);

            _forsendelse.TemporaryDocumentStore.Add(doc);
        }

        /// <summary>
        /// Legg til hovedskjema som pfd
        /// </summary>
        /// <param name="dataType"></param>
        /// <param name="filename"></param>
        /// <param name="content"></param>
        public void AddMainFormPdf(string dataType, string filename, Stream content, List<KeyValuePair<string, string>> ekstraMetadata = null)
        {
            var doc = new SortableSvarUtDocument(SortingSection.MainPdf,
                                                 content,
                                                 CreateValidFileName(filename),
                                                 _getConfig(dataType), 
                                                 ekstraMetadata);

            _forsendelse.TemporaryDocumentStore.Add(doc);
        }

        /// <summary>
        /// Legg til underskjema
        /// </summary>
        /// <param name="dataType"></param>
        /// <param name="filename"></param>
        /// <param name="content"></param>
        /// <param name="dataModelVersion"></param>
        /// <param name="ekstraMetadata"></param>
        public void AddSubformXml(string dataType, string filename, Stream content, string dataModelVersion, List<KeyValuePair<string, string>> ekstraMetadata = null)
        {
            // Gå opp denne ... Hva blir lagt på en subform.
            var config = _getConfig(dataType);

            var fileName = Path.GetFileNameWithoutExtension(filename);
            var fileExtension = Path.GetExtension(filename);
            var newFilename = $"{config.arkivlett1DokumentType}_{fileName}_KAN-INNEHOLDE-PERSONOPPLYSNINGER{fileExtension}";

            var doc = new SvarUtFormDocument(dataModelVersion,
                                                      SortingSection.SubformXmls,
                                                      content,
                                                      newFilename,
                                                      config);

            _forsendelse.TemporaryDocumentStore.Add(doc);
        }

        /// <summary>
        /// Legg til underskjema som pdf
        /// </summary>
        /// <param name="dataType"></param>
        /// <param name="filename"></param>
        /// <param name="content"></param>
        /// <param name="ekstraMetadata"></param>
        public void AddSubformPdf(string dataType, string filename, Stream content, List<KeyValuePair<string, string>> ekstraMetadata = null)
        {
            var config = _getConfig(dataType);
            var newFilename = $"{config.arkivlett1DokumentType}_{filename}";

            var doc = new SortableSvarUtDocument(SortingSection.SubformPdf,
                                                 content,
                                                 newFilename,
                                                 config, 
                                                 ekstraMetadata);

            _forsendelse.TemporaryDocumentStore.Add(doc);
        }

        /// <summary>
        /// Legg til vedlegg
        /// </summary>
        /// <param name="dataType">Datatype definert i altinn eller ftb</param>
        /// <param name="filename"></param>
        /// <param name="mimeType"></param>
        /// <param name="content"></param>
        /// <param name="ekstraMetadata"></param>
        /// <param name="inneholderPersonsensitivInformasjon"></param>
        public void AddAttachment(string dataType, string filename, string mimeType, Stream content, List<KeyValuePair<string, string>> ekstraMetadata = null, bool inneholderPersonsensitivInformasjon = false)
        {
            AddDocument(SortingSection.Attachments, dataType, filename, mimeType, content, ekstraMetadata, inneholderPersonsensitivInformasjon);
        }

        private void AddDocument(SortingSection documentSection, string dataType, string filename, string mimeType, Stream content, List<KeyValuePair<string, string>> ekstraMetadata = null, bool inneholderPersonsensitivInformasjon = false)
        {
            var config = _getConfig(dataType);

            var doc = new SortableSvarUtDocument(documentSection,
                                                 content,
                                                 CreateFileName(dataType, filename),
                                                 config, ekstraMetadata, inneholderPersonsensitivInformasjon);
            doc.MimeType = mimeType;
            _forsendelse.TemporaryDocumentStore.Add(doc);
        }

        private string CreateFileName(string dataType, string filename)
        {
            var documentType = _getConfig(dataType).arkivlett1DokumentType;
            filename = documentType + "_" + dataType + "_" + filename;
            return CreateValidFileName(filename);
        }

        private string CreateValidFileName(string filename)
        {
            filename = GetValidFileName(filename);
            //Max 226 tegn
            return EnforceFilenameLength(filename);
        }

        /// <summary>
        /// Fix the filename by replacing invalid characters and enforcing maximum length.
        /// </summary>
        /// <param name="filename">The filename to fix.</param>
        /// <returns>The fixed filename.</returns>
        private string GetValidFileName(string filename)
        {
            //Replace /\"<>?*|:
            filename = filename.Replace("/", "-");
            filename = filename.Replace(@"\", "-");
            filename = filename.Replace("<", "-");
            filename = filename.Replace(">", "-");
            filename = filename.Replace("?", "-");
            filename = filename.Replace("*", "-");
            filename = filename.Replace("|", "-");

            //Max 226 characters
            return EnforceFilenameLength(filename);
        }

        private const int MaxFilenameLength = 226;

        private static string EnforceFilenameLength(string filename)
        {
            if (filename.Length > MaxFilenameLength)
            {
                var filenameWoExt = Path.GetFileNameWithoutExtension(filename);
                var fileExtension = Path.GetExtension(filename);

                var reducedFilename = filenameWoExt.Substring(0, MaxFilenameLength - fileExtension.Length);
                filename = $"{reducedFilename}{fileExtension}";
            };
            return filename;
        }
    }

    /// <summary>
    /// Dokument som inneholder personsensitiv informasjon
    /// </summary>
    internal class SensitiveSvarUtDocumentForm : SvarUtFormDocument
    {
        public SensitiveSvarUtDocumentForm(string dataModelVersion,
                                           SortingSection sortingSection,
                                           Stream content,
                                           string filename,
                                           (string dataType, string displayName, string arkivlett1DokumentType, string arkivlett2DokumentType, string Category) dataTypeConfig)
            : base(dataModelVersion, sortingSection, content, filename, dataTypeConfig)
        {
            InneholderPersonsensitivInformasjon = true;
        }
    }

    internal class SortableSvarUtDocument : SvarUtDokument
    {
        [JsonIgnore]
        public SortingSection SortingSection { get; set; }

        public SortableSvarUtDocument(SortingSection sortingSection,
                                      Stream content,
                                      string filename,
                                      (string dataType, string displayName, string arkivlett1DokumentType, string arkivlett2DokumentType, string Category) dataTypeConfig, List<KeyValuePair<string, string>> ekstraMetadata = null, bool inneholderPersonsensitivInformasjon = false)
        {
            SortingSection = sortingSection;
            DocumentContent = content;
            Filnavn = filename?.Trim();
            DokumentType = dataTypeConfig.arkivlett1DokumentType;
            EkstraMetadata = new List<Entry>();

            if (ekstraMetadata != null)
            {
                foreach (var metadata in ekstraMetadata)
                {
                    EkstraMetadata.Add(new Entry() { Key = metadata.Key, Value = metadata.Value });
                }
            }

            if (inneholderPersonsensitivInformasjon)
                InneholderPersonsensitivInformasjon = true;

            if (dataTypeConfig != default)
            {
                if (!string.IsNullOrEmpty(dataTypeConfig.dataType))
                    EkstraMetadata.Add(new Entry() { Key = "DatatypeId", Value = dataTypeConfig.dataType });

                if (!string.IsNullOrEmpty(dataTypeConfig.arkivlett1DokumentType))
                    EkstraMetadata.Add(new Entry() { Key = "DokumenttypeArkivlettV1", Value = dataTypeConfig.arkivlett1DokumentType });

                if (!string.IsNullOrEmpty(dataTypeConfig.arkivlett2DokumentType))
                    EkstraMetadata.Add(new Entry() { Key = "DokumenttypeArkivlettV2", Value = dataTypeConfig.arkivlett2DokumentType });

                if (!string.IsNullOrEmpty(dataTypeConfig.Category))
                    EkstraMetadata.Add(new Entry() { Key = "VedleggKategori", Value = dataTypeConfig.Category });

            }
        }
    }

    internal class SvarUtFormDocument : SortableSvarUtDocument
    {
        public SvarUtFormDocument(string dataModelVersion,
                                  SortingSection sortingSection,
                                  Stream content,
                                  string filename,
                                  (string dataType, string displayName, string arkivlett1DokumentType, string arkivlett2DokumentType, string Category) dataTypeConfig, List<KeyValuePair<string, string>> ekstraMetadata = null)
            : base(sortingSection, content, filename, dataTypeConfig, ekstraMetadata)
        {
            if (!string.IsNullOrEmpty(dataModelVersion))
            {
                EkstraMetadata.Add(new Entry() { Key = "DatatypeVersjon", Value = dataModelVersion });
                EkstraMetadata.Add(new Entry() { Key = "SkjemaVersjon", Value = dataModelVersion }); // Avsjekk om noen bruker denne; blir deprecated etterhvert
            }
        }
    }

    internal class SvarUtForsendelse : Forsendelse
    {
        internal List<SortableSvarUtDocument> TemporaryDocumentStore { get; set; }
    }

    internal enum SortingSection
    {
        MainPdf,
        Attachments,
        SubformXmls,
        SubformPdf,
        MainXml
    }
}