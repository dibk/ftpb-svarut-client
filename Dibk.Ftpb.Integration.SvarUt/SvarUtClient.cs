﻿using Dibk.Ftpb.Integration.SvarUt.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Integration.SvarUt
{
    public class SvarUtClient : ISvarUtClient
    {
        private readonly ILogger<SvarUtClient> _logger;
        private HttpClient _httpClient { get; set; }
        private AuthenticationHeaderValue AuthenticationHeader { get; set; }
        private int _defaultTimeoutMilliseconds = 100000;

        public SvarUtClient(HttpClient httpClient, ILogger<SvarUtClient> logger)
        {
            _httpClient = httpClient;
            _logger = logger;
        }

        public void SetAuthentication(string userName, string password)
        {
            var auth = Encoding.ASCII.GetBytes($"{userName}:{password}");
            AuthenticationHeader = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(auth));
        }

        /// <summary>
        /// Retrieves a collection of forsendelsestyper from SvarUt.
        /// Uri: /tjenester/api/forsendelse/v1/forsendelseTyper
        /// </summary>
        /// <returns cref="ForsendelseTyperResult">Returns what is found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        public Task<ForsendelseTyperResult> GetForsendelsesTyperAsync()
        {
            using (var cts = GetDefaultCancellationTokenSource())
                return GetForsendelsesTyperAsync(cts.Token);
        }

        /// <summary>
        /// Retrieves a collection of forsendelsestyper from SvarUt.
        /// Uri: /tjenester/api/forsendelse/v1/forsendelseTyper
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns cref="ForsendelseTyperResult">Returns what is found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        public async Task<ForsendelseTyperResult> GetForsendelsesTyperAsync(CancellationToken cancellationToken)
        {
            var requestUri = "/tjenester/api/forsendelse/v1/forsendelseTyper";
            try
            {
                var message = new HttpRequestMessage(HttpMethod.Get, requestUri);
                message.Headers.Authorization = AuthenticationHeader;
                var response = await _httpClient.SendAsync(message, cancellationToken);

                if (response.IsSuccessStatusCode)
                {
                    var jsonResult = await response.Content.ReadAsStringAsync();
                    var forsendelseTyper = ForsendelseTyperResult.FromJson(jsonResult);

                    return forsendelseTyper;
                }
                else
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                    {
                        _logger.LogDebug($"`{_httpClient.BaseAddress}{requestUri}` was not found");
                        return null;
                    }

                    throw await GetHttpException(response, requestUri);
                }
            }
            catch (SvarUtRequestException) { throw; }
            catch (Exception e)
            {
                _logger.LogError(e, $"Unexpected error when requesting `{_httpClient.BaseAddress}{requestUri}`");
                throw;
            }
        }

        /// <summary>
        /// Sends a forsendelse to SvarUt and returns the id.
        /// Uri: /tjenester/api/forsendelse/v1/sendForsendelse
        /// </summary>
        /// <param cref="Forsendelse" name="forsendelse">A complete forsendelse</param>
        /// <returns cref="ForsendelsesId">The SvarUt forsendelse id</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when constraints in the 'Forsendelse' object are broken</exception>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        public Task<ForsendelsesId> SendForsendelseAsync(Forsendelse forsendelse)
        {
            return SendForsendelseAsync(forsendelse, CancellationToken.None);
        }

        /// <summary>
        /// Sends a forsendelse to SvarUt and returns the id.
        /// Uri: /tjenester/api/forsendelse/v1/sendForsendelse
        /// </summary>
        /// <param cref="Forsendelse" name="forsendelse">A complete forsendelse</param>
        /// <param name="cancellationToken"></param>
        /// <returns cref="ForsendelsesId">The SvarUt forsendelse id</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when constraints in the 'Forsendelse' object are broken</exception>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        public async Task<ForsendelsesId> SendForsendelseAsync(Forsendelse forsendelse, CancellationToken cancellationToken)
        {
            var requestUri = "/tjenester/api/forsendelse/v1/sendForsendelse";

            return await RequestSendForsendelse(requestUri, forsendelse, cancellationToken);
        }

        /// <summary>
        /// Sends a forsendelse to SvarUt and returns the id.
        /// Uri: /tjenester/api/forsendelse/v1/{id}/sendForsendelse
        /// </summary>
        /// <param name="forsendelseId">ForsendelseId created by using GetNyForsendelseId</param>
        /// <param cref="Forsendelse" name="forsendelse">A complete forsendelse</param>
        /// <returns cref="ForsendelsesId">The SvarUt forsendelse id</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when constraints in the 'Forsendelse' object are broken</exception>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        public Task<ForsendelsesId> SendForsendelseAsync(string forsendelseId, Forsendelse forsendelse)
        {
            return SendForsendelseAsync(forsendelseId, forsendelse, CancellationToken.None);
        }

        /// <summary>
        /// Sends a forsendelse to SvarUt and returns the id.
        /// Uri: /tjenester/api/forsendelse/v1/{id}/sendForsendelse
        /// </summary>
        /// <param name="forsendelseId">ForsendelseId created by using GetNyForsendelseId</param>
        /// <param cref="Forsendelse" name="forsendelse">A complete forsendelse</param>
        /// <param name="cancellationToken"></param>
        /// <returns cref="ForsendelsesId">The SvarUt forsendelse id</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when constraints in the 'Forsendelse' object are broken</exception>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        public async Task<ForsendelsesId> SendForsendelseAsync(string forsendelseId, Forsendelse forsendelse, CancellationToken cancellationToken)
        {
            var requestUri = $"/tjenester/api/forsendelse/v1/{forsendelseId}/sendForsendelse";

            return await RequestSendForsendelse(requestUri, forsendelse, cancellationToken);
        }

        private async Task<ForsendelsesId> RequestSendForsendelse(string requestUri, Forsendelse forsendelse, CancellationToken cancellationToken)
        {
            MultipartContent payload = null;

            try
            {
                _logger.LogDebug("Creates SvarUt forsendelse payload");
                var mpb = new ForsendelseMultipartContentBuilder(forsendelse, _logger);

                if (forsendelse.Dokumenter != null)
                {
                    _logger.LogDebug($"Adds #{forsendelse.Dokumenter.Count} to forsendelse payload");
                    foreach (var dokument in forsendelse.Dokumenter)
                    {
                        _logger.LogDebug($"Adds {dokument.Filnavn} to forsendelse payload");
                        mpb.AddDataElement(dokument.Filnavn, dokument.DocumentContent, "application/octet-stream");
                    }
                }

                payload = mpb.Build();
                _logger.LogDebug("SvarUt forsendelse payload created");
            }
            catch (SvarUtRequestException) { throw; }
            catch (Exception e)
            {
                _logger.LogError(e, $"Unexpected error building payload for request to `{_httpClient.BaseAddress}{requestUri}`");
                throw;
            }

            try
            {
                var message = new HttpRequestMessage(HttpMethod.Post, requestUri);
                message.Content = payload;
                message.Headers.Authorization = AuthenticationHeader;

                var response = await _httpClient.SendAsync(message, cancellationToken);

                if (response.IsSuccessStatusCode)
                {
                    _logger.LogDebug($"SvarUt accepted forsendelse on `{_httpClient.BaseAddress}{requestUri}`");

                    var jsonResult = await response.Content.ReadAsStringAsync();
                    _logger.LogDebug(jsonResult);

                    var forsendelsesId = JsonConvert.DeserializeObject<ForsendelsesId>(jsonResult);

                    _logger.LogTrace("Forsendelse {ForsendelsesId} succeeded with payload: {ForsendelsePayload}", forsendelsesId?.Id, ForsendelseSerializer.ToJson(forsendelse));

                    return forsendelsesId;
                }
                else
                {
                    _logger.LogTrace("Forsendelse failed with payload: {ForsendelsePayload}", ForsendelseSerializer.ToJson(forsendelse));
                    throw await GetHttpException(response, requestUri);
                }
            }
            catch (SvarUtRequestException) { throw; }
            catch (Exception e)
            {
                _logger.LogError(e, $"Unexpected error when requesting `{_httpClient.BaseAddress}{requestUri}`");
                throw;
            }
        }

        /// <summary>
        /// Retrieves the status of a given forsendelse.
        /// Uri: /tjenester/api/forsendelse/v1/{forsendelseId}/status
        /// </summary>
        /// <param name="forsendelseId">The forsendelse </param>
        /// <returns cref="ForsendelseStatus">Status of the forsendelse. For 404 responses it returns null</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when constraints in the 'ForsendelseStatus' object are broken</exception>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        public Task<ForsendelseStatus> GetForsendelseStatusAsync(string forsendelseId)
        {
            using (var cts = GetDefaultCancellationTokenSource())
                return GetForsendelseStatusAsync(forsendelseId, cts.Token);
        }

        /// <summary>
        /// Retrieves the status of a given forsendelse.
        /// Uri: /tjenester/api/forsendelse/v1/{forsendelseId}/status
        /// </summary>
        /// <param name="forsendelseId">The forsendelse </param>
        /// <param name="cancellationToken"></param>
        /// <returns cref="ForsendelseStatus">Status of the forsendelse. For 404 responses it returns null</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when constraints in the 'ForsendelseStatus' object are broken</exception>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        public async Task<ForsendelseStatus> GetForsendelseStatusAsync(string forsendelseId, CancellationToken cancellationToken)
        {
            var requestUri = $"/tjenester/api/forsendelse/v1/{forsendelseId}/status";
            try
            {
                var message = new HttpRequestMessage(HttpMethod.Get, requestUri);
                message.Headers.Authorization = AuthenticationHeader;
                var response = await _httpClient.SendAsync(message, cancellationToken);

                if (response.IsSuccessStatusCode)
                {
                    var jsonResult = await response.Content.ReadAsStringAsync();
                    var forsendelseStatus = ForsendelseStatus.FromJson(jsonResult);
                    return forsendelseStatus;
                }
                else
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                    {
                        _logger.LogDebug($"`{_httpClient.BaseAddress}{requestUri}` was not found");
                        return null;
                    }
                    else
                        throw await GetHttpException(response, requestUri);
                }
            }
            catch (SvarUtRequestException) { throw; }
            catch (Exception e)
            {
                _logger.LogError(e, $"Unexpected error when requesting `{_httpClient.BaseAddress}{requestUri}`");
                throw;
            }
        }

        /// <summary>
        /// Retrieves statuses for a range of forsendelses.
        /// Uri: /tjenester/api/forsendelse/v1/statuser
        /// </summary>
        /// <param name="forsendelseIder">Range of forsendelseIds</param>
        /// <returns cref="ForsendelseStatusResult">Returns the result if found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        public Task<ForsendelseStatusResult> GetForsendelseStatuserAsync(List<string> forsendelseIder)
        {
            using (var cts = GetDefaultCancellationTokenSource())
                return GetForsendelseStatuserAsync(forsendelseIder, cts.Token);
        }

        /// <summary>
        /// Retrieves statuses for a range of forsendelses.
        /// Uri: /tjenester/api/forsendelse/v1/statuser
        /// </summary>
        /// <param name="forsendelseIder">Range of forsendelseIds</param>
        /// <param name="cancellationToken"></param>
        /// <returns cref="ForsendelseStatusResult">Returns the result if found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        public async Task<ForsendelseStatusResult> GetForsendelseStatuserAsync(List<string> forsendelseIder, CancellationToken cancellationToken)
        {
            var requestUri = $"/tjenester/api/forsendelse/v1/statuser";
            try
            {
                var message = new HttpRequestMessage(HttpMethod.Post, requestUri);
                message.Content = new StringContent(JsonConvert.SerializeObject(forsendelseIder), Encoding.UTF8, "application/json");
                message.Headers.Authorization = AuthenticationHeader;
                var response = await _httpClient.SendAsync(message, cancellationToken);

                if (response.IsSuccessStatusCode)
                {
                    var jsonResult = await response.Content.ReadAsStringAsync();
                    var forsendelsestatus = ForsendelseStatusResult.FromJson(jsonResult);
                    return forsendelsestatus;
                }
                else
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                    {
                        _logger.LogDebug($"`{_httpClient.BaseAddress}{requestUri}` was not found");
                        return null;
                    }
                    else
                        throw await GetHttpException(response, requestUri);
                }
            }
            catch (SvarUtRequestException) { throw; }
            catch (Exception e)
            {
                _logger.LogError(e, $"Unexpected error when requesting `{_httpClient.BaseAddress}{requestUri}`");
                throw;
            }
        }

        /// <summary>
        /// Get forsendelses historikk for a given id.
        /// Uri: /tjenester/api/forsendelse/v1/{forsendelseId}/historikk
        /// </summary>
        /// <param name="forsendelseId"></param>
        /// <returns cref="ForsendelseHistorikk">Returns what is found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        public Task<ForsendelseHistorikk> GetForsendelseHistorikkAsync(string forsendelseId)
        {
            using (var cts = GetDefaultCancellationTokenSource())
                return GetForsendelseHistorikkAsync(forsendelseId, cts.Token);
        }

        /// <summary>
        /// Get forsendelses historikk for a given id.
        /// Uri: /tjenester/api/forsendelse/v1/{forsendelseId}/historikk
        /// </summary>
        /// <param name="forsendelseId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns cref="ForsendelseHistorikk">Returns what is found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        public async Task<ForsendelseHistorikk> GetForsendelseHistorikkAsync(string forsendelseId, CancellationToken cancellationToken)
        {
            var requestUri = $"/tjenester/api/forsendelse/v1/{forsendelseId}/historikk";

            try
            {
                var message = new HttpRequestMessage(HttpMethod.Get, requestUri);
                var response = await _httpClient.SendAsync(message, cancellationToken);

                if (response.IsSuccessStatusCode)
                {
                    var jsonResult = await response.Content.ReadAsStringAsync();
                    var forsendelseHistorikk = ForsendelseHistorikk.FromJson(jsonResult);
                    return forsendelseHistorikk;
                }
                else
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                    {
                        _logger.LogDebug($"`{_httpClient.BaseAddress}{requestUri}` was not found");
                        return null;
                    }
                    else
                        throw await GetHttpException(response, requestUri);
                }
            }
            catch (SvarUtRequestException) { throw; }
            catch (Exception e)
            {
                _logger.LogError(e, $"Unexpected error when requesting `{_httpClient.BaseAddress}{requestUri}`");
                throw;
            }
        }

        /// <summary>
        /// Retrieves forsendelsestyper for a given organizational number.
        /// Uri: /tjenester/api/forsendelse/v1/mottakersystem/{organisasjonsnummer}
        /// </summary>
        /// <param name="organisasjonsnummer"></param>
        /// <returns cref="MottakerForsendelseTyperResult">Returns what is found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        public Task<MottakerForsendelseTyperResult> GetMottakerForsendelseTyperAsync(string organisasjonsnummer)
        {
            using (var cts = GetDefaultCancellationTokenSource())
                return GetMottakerForsendelseTyperAsync(organisasjonsnummer, cts.Token);
        }

        /// <summary>
        /// Retrieves forsendelsestyper for a given organizational number.
        /// Uri: /tjenester/api/forsendelse/v1/mottakersystem/{organisasjonsnummer}
        /// </summary>
        /// <param name="organisasjonsnummer"></param>
        /// <param name="cancellationToken"></param>
        /// <returns cref="MottakerForsendelseTyperResult">Returns what is found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        public async Task<MottakerForsendelseTyperResult> GetMottakerForsendelseTyperAsync(string organisasjonsnummer, CancellationToken cancellationToken)
        {
            var requestUri = $"/tjenester/api/forsendelse/v1/mottakersystem/{organisasjonsnummer}";
            try
            {
                var message = new HttpRequestMessage(HttpMethod.Get, requestUri);
                message.Headers.Authorization = AuthenticationHeader;
                var response = await _httpClient.SendAsync(message, cancellationToken);

                if (response.IsSuccessStatusCode)
                {
                    var jsonResult = await response.Content.ReadAsStringAsync();
                    var mottakerForsendelseTyper = MottakerForsendelseTyperResult.FromJson(jsonResult);
                    return mottakerForsendelseTyper;
                }
                else
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                    {
                        _logger.LogDebug($"`{_httpClient.BaseAddress}{requestUri}` was not found");
                        return null;
                    }
                    else
                        throw await GetHttpException(response, requestUri);
                }
            }
            catch (SvarUtRequestException) { throw; }
            catch (Exception e)
            {
                _logger.LogError(e, $"Unexpected error when requesting `{_httpClient.BaseAddress}{requestUri}`");
                throw;
            }
        }

        /// <summary>
        /// Retreives a list of information about documents which are a part of a 'forsendelse'.
        /// Uri: /tjenester/api/forsendelse/v1/{forsendelseId}/dokumentmetadata
        /// </summary>
        /// <param name="forsendelseId"></param>
        /// <returns cref="DokumentMetadataResult">Returns what is found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        public Task<DokumentMetadataResult> GetDokumentMetadataAsync(string forsendelseId)
        {
            using (var cts = GetDefaultCancellationTokenSource())
                return GetDokumentMetadataAsync(forsendelseId, cts.Token);
        }

        /// <summary>
        /// Retreives a list of information about documents which are a part of a 'forsendelse'.
        /// Uri: /tjenester/api/forsendelse/v1/{forsendelseId}/dokumentmetadata
        /// </summary>
        /// <param name="forsendelseId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns cref="DokumentMetadataResult">Returns what is found. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        public async Task<DokumentMetadataResult> GetDokumentMetadataAsync(string forsendelseId, CancellationToken cancellationToken)
        {
            var requestUri = $"/tjenester/api/forsendelse/v1/{forsendelseId}/dokumentmetadata";
            try
            {
                var message = new HttpRequestMessage(HttpMethod.Get, requestUri);
                message.Headers.Authorization = AuthenticationHeader;
                var response = await _httpClient.SendAsync(message, cancellationToken);

                if (response.IsSuccessStatusCode)
                {
                    var jsonResult = await response.Content.ReadAsStringAsync();
                    var dokumentMetadataResult = DokumentMetadataResult.FromJson(jsonResult);
                    return dokumentMetadataResult;
                }
                else
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                    {
                        _logger.LogDebug($"`{_httpClient.BaseAddress}{requestUri}` was not found");
                        return null;
                    }
                    else
                        throw await GetHttpException(response, requestUri);
                }
            }
            catch (SvarUtRequestException) { throw; }
            catch (Exception e)
            {
                _logger.LogError(e, $"Unexpected error when requesting `{_httpClient.BaseAddress}{requestUri}`");
                throw;
            }
        }

        /// <summary>
        /// Creates a new ForsendelseId in SvarUt.
        /// Uri: /tjenester/api/forsendelse/v1/startNyForsendelse
        /// </summary>
        /// <returns cref="ForsendelsesId">A new ForsendelseId. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        public Task<ForsendelsesId> GetNyForsendelseIdAsync()
        {
            using(var cts = GetDefaultCancellationTokenSource())
                return GetNyForsendelseIdAsync(cts.Token);
        }

        /// <summary>
        /// Creates a new ForsendelseId in SvarUt.
        /// Uri: /tjenester/api/forsendelse/v1/startNyForsendelse
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns cref="ForsendelsesId">A new ForsendelseId. For 404 responses it returns null</returns>
        /// <exception cref="SvarUtRequestException">Thrown when SvarUt reports an error</exception>
        /// <exception cref="HttpRequestException">Thrown when the HttpRequest fails and not an error reported by SvarUt</exception>
        public async Task<ForsendelsesId> GetNyForsendelseIdAsync(CancellationToken cancellationToken)
        {
            var requestUri = $"/tjenester/api/forsendelse/v1/startNyForsendelse";
            try
            {
                var message = new HttpRequestMessage(HttpMethod.Post, requestUri);
                message.Headers.Authorization = AuthenticationHeader;
                message.Content = new MultipartFormDataContent();
                var response = await _httpClient.SendAsync(message, cancellationToken);

                if (response.IsSuccessStatusCode)
                {
                    var jsonResult = await response.Content.ReadAsStringAsync();
                    var forsendelsesId = JsonConvert.DeserializeObject<ForsendelsesId>(jsonResult);
                    return forsendelsesId;
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    _logger.LogDebug($"`{_httpClient.BaseAddress}{requestUri}` was not found");
                    return null;
                }
                else
                {
                    throw await GetHttpException(response, requestUri);
                }
            }
            catch (SvarUtRequestException) { throw; }
            catch (Exception e)
            {
                _logger.LogError(e, $"Unexpected error when requesting `{_httpClient.BaseAddress}{requestUri}`");
                throw;
            }
        }

        private CancellationTokenSource GetDefaultCancellationTokenSource()
        {
            return new CancellationTokenSource(_defaultTimeoutMilliseconds);
        }

        private async Task<SvarUtRequestException> GetHttpException(HttpResponseMessage httpResponseMessage, string requestUri)
        {
            try
            {
                var response = await httpResponseMessage.Content.ReadAsStringAsync();
                var svarUtError = SvarUtError.FromJson(response);
                var requestException = new SvarUtRequestException(httpResponseMessage.StatusCode, svarUtError);
                _logger.LogError(requestException, $"SvarUt gave an error when requesting `{_httpClient.BaseAddress}{requestUri}`. SvarUtError response: {response}");

                return requestException;
            }
            catch
            {
                httpResponseMessage.EnsureSuccessStatusCode();

                return null; //This return should not happen since the status code isn't a successful one..
            }
        }
    }
}