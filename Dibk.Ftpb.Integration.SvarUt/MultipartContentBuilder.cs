﻿using Dibk.Ftpb.Integration.SvarUt.Models;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace Dibk.Ftpb.Integration.SvarUt
{
    public class ForsendelseMultipartContentBuilder
    {
        private MultipartFormDataContent builder;

        public ForsendelseMultipartContentBuilder(Forsendelse forsendelse, ILogger logger = null)
        {
            builder = new MultipartFormDataContent();
            if (forsendelse != null)
            {
                var forsendelseJson = ForsendelseSerializer.ToJson(forsendelse);

                StringContent forsendelseContent = new StringContent(forsendelseJson, Encoding.UTF8, "application/json");

                builder.Add(forsendelseContent, "forsendelse");
            }
        }

        public ForsendelseMultipartContentBuilder AddDataElement(string fileName, Stream stream, string contentType)
        {
            StreamContent streamContent = new StreamContent(stream);
            streamContent.Headers.ContentType = MediaTypeHeaderValue.Parse(contentType);

            builder.Add(streamContent, "filer", fileName);

            return this;
        }

        public ForsendelseMultipartContentBuilder AddDataElement(string elementType, StringContent content)
        {
            builder.Add(content, elementType);

            return this;
        }

        public MultipartFormDataContent Build()
        {
            return builder;
        }
    }
}