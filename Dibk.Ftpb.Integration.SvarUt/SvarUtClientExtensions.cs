﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Text;

namespace Dibk.Ftpb.Integration.SvarUt
{
    public static class SvarUtClientExtensions
    {
        public static IHttpClientBuilder AddSvarutClient(this IServiceCollection services, Uri baseUri)
        {
            return AddSvarUtClient(services, baseUri, null, null);
        }

        public static IHttpClientBuilder AddSvarUtClient(this IServiceCollection services, Uri baseUri, string userName, string password)
        {
            return services.AddHttpClient<ISvarUtClient, SvarUtClient>(opt =>
            {
                opt.BaseAddress = baseUri;
                opt.Timeout = TimeSpan.FromMinutes(16);

                if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
                {
                    var auth = Encoding.ASCII.GetBytes($"{userName}:{password}");
                    opt.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(auth));
                }
            });
        }
    }
}