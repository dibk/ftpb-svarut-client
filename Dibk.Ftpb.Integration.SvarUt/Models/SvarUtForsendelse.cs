﻿using Newtonsoft.Json;
using System.IO;

namespace Dibk.Ftpb.Integration.SvarUt.Models
{
    public class SvarUtForsendelse
    {
    }

    public class SvarUtDokument : Dokument, IDokument
    {
        [JsonIgnore]
        public Stream DocumentContent { get; set; }

        /// <summary>
        /// Forsøker å bestemme MimeType basert på fil-extension dersom den ikke settes.
        /// </summary>
        public override string MimeType
        {
            get
            {
                if (string.IsNullOrEmpty(base.MimeType))
                    return GetMimeType();
                else
                    return base.MimeType;
            }
            set { base.MimeType = value; }
        }

        private string GetMimeType()
        {
            var mimeType = string.Empty;

            MimeTypes.TryGetMimeType(base.Filnavn, out mimeType);
            return mimeType;
        }
    }
}