﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Dibk.Ftpb.Integration.SvarUt.Models
{
    public class ForsendelsesId
    {
        [JsonProperty("forsendelseIder")]
        public List<ForsendelsesId> ForsendelseIder { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }
}