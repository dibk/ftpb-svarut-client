﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.Globalization;

namespace Dibk.Ftpb.Integration.SvarUt.Models
{
    public class ForsendelseTyperResult
    {
        [JsonProperty("forsendelseTyper")]
        public List<string> ForsendelseTyper { get; set; }

        public static ForsendelseTyperResult FromJson(string json) => JsonConvert.DeserializeObject<ForsendelseTyperResult>(json, SvarUt.Models.ForsendelseTyperResultConverter.Settings);
    }

    public static class ForsendelseTyperResultSerialize
    {
        public static string ToJson(this ForsendelseTyperResult self) => JsonConvert.SerializeObject(self, SvarUt.Models.ForsendelseTyperResultConverter.Settings);
    }

    internal static class ForsendelseTyperResultConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}