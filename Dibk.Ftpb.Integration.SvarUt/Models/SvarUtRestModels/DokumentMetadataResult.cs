﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.Globalization;

namespace Dibk.Ftpb.Integration.SvarUt.Models
{
    public class DokumentMetadataResult
    {
        [JsonProperty("dokumentMetadata")]
        public List<DokumentMetadata> DokumentMetadata { get; set; }

        public static DokumentMetadataResult FromJson(string json) => JsonConvert.DeserializeObject<DokumentMetadataResult>(json, SvarUt.Models.DokumentMetadataResultConverter.Settings);
    }

    public class DokumentMetadata
    {
        /// <summary>
        /// Noark dokumenttype. M083
        /// </summary>
        [JsonProperty("dokumentType")]
        public string DokumentType { get; set; }

        [JsonProperty("filnavn", NullValueHandling = NullValueHandling.Ignore)]
        public string Filnavn { get; set; }

        [JsonProperty("kanSigneres", NullValueHandling = NullValueHandling.Ignore)]
        public bool? KanSigneres { get; set; }

        [JsonProperty("mimeType", NullValueHandling = NullValueHandling.Ignore)]
        public string MimeType { get; set; }

        [JsonProperty("nedlasningsUrl")]
        public string NedlasningsUrl { get; set; }

        /// <summary>
        /// Sha256 hash av filinnholdet
        /// </summary>
        [JsonProperty("sha256hash")]
        public string Sha256Hash { get; set; }

        [JsonProperty("signeringsUrl")]
        public string SigneringsUrl { get; set; }

        [JsonProperty("sizeInBytes")]
        public double? SizeInBytes { get; set; }

        [JsonProperty("ekstraMetadata")]
        public List<Entry> EkstraMetadata { get; set; }
    }

    public static class DokumentMetadataResultSerialize
    {
        public static string ToJson(this DokumentMetadataResult self) => JsonConvert.SerializeObject(self, SvarUt.Models.DokumentMetadataResultConverter.Settings);
    }

    internal static class DokumentMetadataResultConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}