﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.Globalization;

namespace Dibk.Ftpb.Integration.SvarUt.Models
{
    public class SigneringsHistorikk
    {
        [JsonProperty("forsendelsesId")]
        public ForsendelsesId ForsendelsesId { get; set; }

        [JsonProperty("logg")]
        public List<SigneringsLogg> Logg { get; set; }

        public static SigneringsHistorikk FromJson(string json) => JsonConvert.DeserializeObject<SigneringsHistorikk>(json, SvarUt.Models.SigneringsHistorikkConverter.Settings);
    }

    public class SigneringsLogg
    {
        [JsonProperty("hendelse", NullValueHandling = NullValueHandling.Ignore)]
        public string Hendelse { get; set; }

        [JsonProperty("tidspunkt", NullValueHandling = NullValueHandling.Ignore)]
        public string Tidspunkt { get; set; }

        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }
    }

    public static class SigneringsHistorikkSerialize
    {
        public static string ToJson(this SigneringsHistorikk self) => JsonConvert.SerializeObject(self, SvarUt.Models.SigneringsHistorikkConverter.Settings);
    }

    internal static class SigneringsHistorikkConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}