﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.Globalization;

namespace Dibk.Ftpb.Integration.SvarUt.Models
{
    public class ForsendelseHistorikk
    {
        [JsonProperty("hendelsesLogger", NullValueHandling = NullValueHandling.Ignore)]
        public List<HendelsesLogg> HendelsesLogger { get; set; }

        public static ForsendelseHistorikk FromJson(string json) => JsonConvert.DeserializeObject<ForsendelseHistorikk>(json, SvarUt.Models.ForsendelseHistorikkConverter.Settings);
    }

    public class HendelsesLogg
    {
        [JsonProperty("hendelse", NullValueHandling = NullValueHandling.Ignore)]
        public string Hendelse { get; set; }

        [JsonProperty("tidspunkt", NullValueHandling = NullValueHandling.Ignore)]
        public string Tidspunkt { get; set; }
    }

    public static class ForsendelseHistorikkSerialize
    {
        public static string ToJson(this ForsendelseHistorikk self) => JsonConvert.SerializeObject(self, SvarUt.Models.ForsendelseHistorikkConverter.Settings);
    }

    internal static class ForsendelseHistorikkConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}