﻿using Dibk.Ftpb.Integration.SvarUt.Models.SvarUtRestModels.JsonConverters;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Globalization;

namespace Dibk.Ftpb.Integration.SvarUt.Models
{
    public class SvarUtError
    {
        /// <summary>
        /// Timestamp when error occurred
        /// </summary>
        [JsonConverter(typeof(SecondsFrom1970TimeConverter))]
        [JsonProperty("timestamp")]
        public DateTime? TimeStamp { get; set; }

        /// <summary>
        /// HTTP-statuskode, i.e. 400 eller 500.
        /// </summary>
        [JsonProperty("status")]
        public int StatusCode { get; set; }

        /// <summary>
        /// Description of the HTTP-status, i.e. Bad-Request, Internal Server Error
        /// </summary>
        [JsonProperty("error")]
        public string Error { get; set; }

        /// <summary>
        /// Internal id used to located information related to the error
        /// </summary>
        [JsonProperty("errorId")]
        public string ErrorId { get; set; }

        /// <summary>
        /// The path the request was made on
        /// </summary>
        [JsonProperty("path")]
        public string Path { get; set; }

        /// <summary>
        /// Will only be used on some rare occations
        /// </summary>
        [JsonProperty("originalPath")]
        public string OriginalPath { get; set; }

        /// <summary>
        /// Human readable description of the error
        /// </summary>
        [JsonProperty("message", NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        /// <summary>
        /// Service specific error code. Will on most occasions not have a value
        /// </summary>
        [JsonProperty("errorcode")]
        public string ErrorCode { get; set; }

        /// <summary>
        /// Details of the 'ErrorCode' prop in JSON-format
        /// </summary>
        [JsonProperty("errorJson")]
        public string ErrorJson { get; set; }

        public static SvarUtError FromJson(string json) => JsonConvert.DeserializeObject<SvarUtError>(json, SvarUt.Models.SvarUtErrorConverter.Settings);
    }

    public static class SvarUtErrorSerialize
    {
        public static string ToJson(this SvarUtError self) => JsonConvert.SerializeObject(self, SvarUt.Models.SvarUtErrorConverter.Settings);
    }

    internal static class SvarUtErrorConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}