﻿using Newtonsoft.Json;
using System;

namespace Dibk.Ftpb.Integration.SvarUt.Models.SvarUtRestModels.JsonConverters
{
    public class SecondsFrom1970TimeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var t = (long)reader.Value;
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(t);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var date = (DateTime)value;
            var milliseconds = (date - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
            writer.WriteValue(Convert.ToInt64(milliseconds));
        }
    }
}