﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Globalization;

namespace Dibk.Ftpb.Integration.SvarUt.Models
{
    public class LestAv
    {
        [JsonProperty("datoLest")]
        public DateTime? DatoLest { get; set; }

        [JsonProperty("lestAvFodselsNummer")]
        public string LestAvFodselsNummer { get; set; }

        [JsonProperty("navnPaEksterntSystem")]
        public string NavnPaEksterntSystem { get; set; }

        public static LestAv FromJson(string json) => JsonConvert.DeserializeObject<LestAv>(json, SvarUt.Models.LestAvConverter.Settings);
    }

    public static class LestAvSerialize
    {
        public static string ToJson(this LestAv self) => JsonConvert.SerializeObject(self, SvarUt.Models.LestAvConverter.Settings);
    }

    internal static class LestAvConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}