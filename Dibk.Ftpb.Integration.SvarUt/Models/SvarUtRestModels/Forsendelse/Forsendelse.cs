﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Dibk.Ftpb.Integration.SvarUt.Models
{
    public class Forsendelse
    {
        /// <summary>
        /// Streng som forteller system med versjon
        /// </summary>
        [JsonProperty("avgivendeSystem")]
        public string AvgivendeSystem { get; set; }

        /// <summary>
        /// Metadata om dokumentene som skal være med forsendelsen
        /// </summary>
        [JsonProperty("dokumenter")]
        public List<IDokument> Dokumenter { get; set; }

        /// <summary>
        /// Disse kan også laste ned filene for forsendelsen. Hvis avsender indexerer forsendelsen i
        /// FIKS-Innsyn blir forsendelsen også tilgjengelig for disse.
        /// </summary>
        [JsonProperty("eksponertFor", NullValueHandling = NullValueHandling.Ignore)]
        public List<Digitaladresse> EksponertFor { get; set; }

        /// <summary>
        /// ID i avleverende system, eller annen id som identifiserer forsendelsen. Kan brukes til å
        /// finne forsendelseId
        /// </summary>
        [JsonProperty("eksternReferanse")]
        public string EksternReferanse { get; set; }

        /// <summary>
        /// ForsendelseType, kan brukes til å kategorisere forsendelsen. Det er kun typene du får
        /// returnert fra /tjenester/api/forsendelse/v1/forsendelseTyper som kan brukes til å route
        /// meldingen til korrekt fagsystem via SvarInn
        /// </summary>
        [JsonProperty("forsendelsesType")]
        public string ForsendelsesType { get; set; }

        /// <summary>
        /// Forsendelser blir gruppert på konteringskode i faktura.
        /// </summary>
        [JsonProperty("konteringsKode")]
        public string KonteringsKode { get; set; }

        /// <summary>
        /// Skal en kreve nivå4 innlogging for å lese forsendelsen. Brukes på sensetive forsendelser.
        /// </summary>
        [JsonProperty("krevNiva4Innlogging", NullValueHandling = NullValueHandling.Ignore)]
        public bool? KrevNiva4Innlogging { get; set; }

        /// <summary>
        /// Er filene kryptert
        /// </summary>
        [JsonProperty("kryptert", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Kryptert { get; set; }

        /// <summary>
        /// Vil ikke under noen omstendigheter blir printet. INGEN GARANTI OM LEVERANSE. Vi prøver få
        /// godt vi kan, men funker ingen digitale kanaler får avsender ingen tilbakemelding. Brukes
        /// til typisk reklame.
        /// </summary>
        [JsonProperty("kunDigitalLevering", NullValueHandling = NullValueHandling.Ignore)]
        public bool? KunDigitalLevering { get; set; }

        /// <summary>
        /// Lenker som er med forsendelsen, disse blir vist i meldingen i Altinn, eBoks/Digipost og
        /// andre fremtidige kanaler.
        /// </summary>
        [JsonProperty("lenker")]
        public List<Lenke> Lenker { get; set; }

        /// <summary>
        /// Noark metadata avsender vet om mottakersystem.
        /// </summary>
        [JsonProperty("metadataForImport")]
        public NoarkMetadataForImport MetadataForImport { get; set; }

        /// <summary>
        /// Noark metadata fra avsendersystemet
        /// </summary>
        [JsonProperty("metadataFraAvleverendeSystem")]
        public NoarkMetadataFraAvleverendeSaksSystem MetadataFraAvleverendeSystem { get; set; }

        /// <summary>
        /// Mottaker av forsendelsen
        /// </summary>
        [JsonProperty("mottaker")]
        public Adresse Mottaker { get; set; }

        /// <summary>
        /// Signaturtype, angir hvilken type signatur du får tilbake. See mer her:
        /// https://samarbeid.difi.no/felleslosninger/esignering
        /// </summary>
        [JsonProperty("signaturType")]
        public SignaturType? SignaturType { get; set; }

        /// <summary>
        /// Hvis forsendelsen kan signeres utløper muligheten til å signere etter dette tidspunketet.
        /// </summary>
        [JsonProperty("signeringUtloper")]
        public DateTime? SigneringUtloper { get; set; }

        /// <summary>
        /// Dette er svar på en tidligere forsendelse.
        /// </summary>
        [JsonProperty("svarPaForsendelse")]
        public ForsendelsesId SvarPaForsendelse { get; set; }

        /// <summary>
        /// Skal det ligge med link til edialog svar på denne forsendelsen.
        /// </summary>
        [JsonProperty("svarPaForsendelseLink", NullValueHandling = NullValueHandling.Ignore)]
        public bool? SvarPaForsendelseLink { get; set; }

        /// <summary>
        /// Svar på denne forsendelsen sendes til.
        /// </summary>
        [JsonProperty("svarSendesTil", NullValueHandling = NullValueHandling.Ignore)]
        public Adresse SvarSendesTil { get; set; }

        [JsonProperty("tittel")]
        public string Tittel { get; set; }

        /// <summary>
        /// Hvordan skal forsendelsen printes
        /// </summary>
        [JsonProperty("utskriftsKonfigurasjon", NullValueHandling = NullValueHandling.Ignore)]
        public UtskriftsKonfigurasjon UtskriftsKonfigurasjon { get; set; }
    }

    public class Dokument
    {
        /// <summary>
        /// Noark dokumenttype. M083
        /// </summary>
        [JsonProperty("dokumentType")]
        public string DokumentType { get; set; }

        /// <summary>
        /// Denne filen skal ikke inngå i print. Brukes om du sender andre formater enn pdf som ikke
        /// kan eller skal printes.
        /// </summary>
        [JsonProperty("ekskluderesFraUtskrift", NullValueHandling = NullValueHandling.Ignore)]
        public bool? EkskluderesFraUtskrift { get; set; }

        /// <summary>
        /// Filnavn, kan ikke inneholde: " &lt; &gt; ? * | : \ /
        /// </summary>
        [JsonProperty("filnavn")]
        [JsonConverter(typeof(FilnavnMinMaxLengthCheckConverter))]
        public string Filnavn { get; set; }

        /// <summary>
        /// Liste med sidetall som skal printes på gult giroark. Digital versjon vil få grått
        /// giroark. Første side er 1. Ved dobbelsidig print må giroarket ha en tom bakside.
        /// </summary>
        [JsonProperty("giroarkSider")]
        public List<long> GiroarkSider { get; set; }

        /// <summary>
        /// Gyldig mimetype for filen.
        /// </summary>
        [JsonProperty("mimeType")]
        public virtual string MimeType { get; set; }

        /// <summary>
        /// Om filen kan signeres
        /// </summary>
        [JsonProperty("skalSigneres", NullValueHandling = NullValueHandling.Ignore)]
        public bool? SkalSigneres { get; set; }

        /// <summary>
        /// Brukes til andre data som avsender vil ha med i forsendelsen.
        /// </summary>
        [JsonProperty("ekstraMetadata")]
        public List<Entry> EkstraMetadata { get; set; }

        /// <summary>
        /// Om filen inneholder personsensitiv informasjon
        /// </summary>
        [JsonProperty("inneholderPersonsensitivInformasjon", NullValueHandling = NullValueHandling.Ignore)]
        public bool? InneholderPersonsensitivInformasjon { get; set; }
    }

    public class Digitaladresse
    {
        [JsonProperty("fodselsNummer", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(FodselsnummerMinMaxLengthCheckConverter))]
        public string FodselsNummer { get; set; }

        [JsonProperty("organisasjonsNummer", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(OrgnrMinMaxLengthCheckConverter))]
        public string OrganisasjonsNummer { get; set; }
    }

    public class Lenke
    {
        /// <summary>
        /// En paragraf med tekst som vises før lenken.
        /// </summary>
        [JsonProperty("ledeTekst")]
        public string LedeTekst { get; set; }

        /// <summary>
        /// Urlen
        /// </summary>
        [JsonProperty("urlLenke")]
        public string UrlLenke { get; set; }

        /// <summary>
        /// Tekst på urlen.
        /// </summary>
        [JsonProperty("urlTekst")]
        public string UrlTekst { get; set; }
    }

    public class NoarkMetadataForImport
    {
        [JsonProperty("dokumentetsDato")]
        public DateTime? DokumentetsDato { get; set; }

        /// <summary>
        /// Kan settet til f.eks U hvis det skal importeres som et utgående dokument.
        /// </summary>
        [JsonProperty("journalPostType")]
        public string JournalPostType { get; set; }

        [JsonProperty("journalStatus")]
        public string JournalStatus { get; set; }

        [JsonProperty("saksAar", NullValueHandling = NullValueHandling.Ignore)]
        public long? SaksAar { get; set; }

        [JsonProperty("saksSekvensNummer", NullValueHandling = NullValueHandling.Ignore)]
        public long? SaksSekvensNummer { get; set; }

        [JsonProperty("tittel")]
        public string Tittel { get; set; }
    }

    public class NoarkMetadataFraAvleverendeSaksSystem
    {
        [JsonProperty("dokumentetsDato")]
        public DateTime? DokumentetsDato { get; set; }

        /// <summary>
        /// Brukes til andre data som avsender vil ha med i forsendelsen.
        /// </summary>
        [JsonProperty("ekstraMetadata", NullValueHandling = NullValueHandling.Ignore)]
        public List<Entry> EkstraMetadata { get; set; }

        [JsonProperty("journalAar", NullValueHandling = NullValueHandling.Ignore)]
        public long? JournalAar { get; set; }

        [JsonProperty("journalDato")]
        public DateTime? JournalDato { get; set; }

        [JsonProperty("journalPostNummer", NullValueHandling = NullValueHandling.Ignore)]
        public long? JournalPostNummer { get; set; }

        [JsonProperty("journalPostType")]
        public string JournalPostType { get; set; }

        [JsonProperty("journalSekvensNummer", NullValueHandling = NullValueHandling.Ignore)]
        public long? JournalSekvensNummer { get; set; }

        [JsonProperty("journalStatus")]
        public string JournalStatus { get; set; }

        [JsonProperty("saksAar", NullValueHandling = NullValueHandling.Ignore)]
        public long? SaksAar { get; set; }

        [JsonProperty("saksBehandler")]
        public string SaksBehandler { get; set; }

        [JsonProperty("saksSekvensNummer", NullValueHandling = NullValueHandling.Ignore)]
        public long? SaksSekvensNummer { get; set; }

        [JsonProperty("tittel")]
        public string Tittel { get; set; }
    }

    public class Entry
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }

    /// <summary>
    /// Mottaker av forsendelsen
    ///
    /// Svar på denne forsendelsen sendes til.
    /// </summary>
    public class Adresse
    {
        [JsonProperty("digitalAdresse", NullValueHandling = NullValueHandling.Ignore)]
        public Digitaladresse DigitalAdresse { get; set; }

        [JsonProperty("postAdresse", NullValueHandling = NullValueHandling.Ignore)]
        public PostAdresse PostAdresse { get; set; }
    }

    /// <summary>
    /// Se posten: https://www.posten.no/sende/klargjoring/adressering Dette blir printet på
    /// konvolutten i denne rekkefølge. Postnr og poststed på samme linje.
    /// </summary>
    public class PostAdresse
    {
        [JsonProperty("adresse1")]
        public string Adresse1 { get; set; }

        [JsonProperty("adresse2")]
        public string Adresse2 { get; set; }

        [JsonProperty("adresse3")]
        public string Adresse3 { get; set; }

        /// <summary>
        /// Gyldige navn på Norge vil gi norsk porto. Gyldige land i europa vil gi Europa porto.
        /// Andre verdier gir Verdens porto.
        /// </summary>
        [JsonProperty("land")]
        public string Land { get; set; }

        [JsonProperty("navn")]
        public string Navn { get; set; }

        [JsonProperty("postNummer")]
        public string PostNummer { get; set; }

        [JsonProperty("postSted")]
        public string PostSted { get; set; }
    }

    /// <summary>
    /// Hvordan skal forsendelsen printes
    /// </summary>
    public class UtskriftsKonfigurasjon
    {
        [JsonProperty("tosidig", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Tosidig { get; set; }

        [JsonProperty("utskriftMedFarger", NullValueHandling = NullValueHandling.Ignore)]
        public bool? UtskriftMedFarger { get; set; }
    }

    public enum SignaturType
    { AutentisertSignatur, AvansertSignatur };

    public static class ForsendelseSerializer
    {
        public static string ToJson(this Forsendelse self) => JsonConvert.SerializeObject(self, SvarUt.Models.ForsendelseConverter.Settings);
    }

    internal static class ForsendelseConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                SignaturTypeConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class FilnavnMinMaxLengthCheckConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(string);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            var value = serializer.Deserialize<string>(reader);
            if (value.Length <= 226)
            {
                return value;
            }
            throw new ArgumentOutOfRangeException(reader.Path, value, "Filename cannot exceed 226 characters");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (string)untypedValue;
            if (value.Length <= 226)
            {
                serializer.Serialize(writer, value);
                return;
            }
            throw new ArgumentOutOfRangeException(writer.Path, value, "Filename cannot exceed 226 characters");
        }

        public static readonly FilnavnMinMaxLengthCheckConverter Singleton = new FilnavnMinMaxLengthCheckConverter();
    }

    internal class FodselsnummerMinMaxLengthCheckConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(string);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            var value = serializer.Deserialize<string>(reader);
            if (value.Length >= 11 && value.Length <= 11)
            {
                return value;
            }
            throw new ArgumentOutOfRangeException(reader.Path, value, "Fødselsnummer must be 11 characters");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (string)untypedValue;
            if (value.Length >= 11 && value.Length <= 11)
            {
                serializer.Serialize(writer, value);
                return;
            }
            throw new ArgumentOutOfRangeException(writer.Path, value, "Fødselsnummer must be 11 characters");
        }

        public static readonly FodselsnummerMinMaxLengthCheckConverter Singleton = new FodselsnummerMinMaxLengthCheckConverter();
    }

    internal class OrgnrMinMaxLengthCheckConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(string);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            var value = serializer.Deserialize<string>(reader);
            if (value.Length >= 9 && value.Length <= 9)
            {
                return value;
            }
            throw new ArgumentOutOfRangeException(reader.Path, value, "Orgnr must be 9 characters");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (string)untypedValue;
            if (value.Length >= 9 && value.Length <= 9)
            {
                serializer.Serialize(writer, value);
                return;
            }
            throw new ArgumentOutOfRangeException(writer.Path, value, "Orgnr must be 9 characters");
        }

        public static readonly OrgnrMinMaxLengthCheckConverter Singleton = new OrgnrMinMaxLengthCheckConverter();
    }

    internal class SignaturTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(SignaturType) || t == typeof(SignaturType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "AUTENTISERT_SIGNATUR":
                    return SignaturType.AutentisertSignatur;

                case "AVANSERT_SIGNATUR":
                    return SignaturType.AvansertSignatur;
            }
            throw new ArgumentOutOfRangeException(reader.Path, value, $"Cannot unmarshal value {value} to type SignaturType");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (SignaturType)untypedValue;
            switch (value)
            {
                case SignaturType.AutentisertSignatur:
                    serializer.Serialize(writer, "AUTENTISERT_SIGNATUR");
                    return;

                case SignaturType.AvansertSignatur:
                    serializer.Serialize(writer, "AVANSERT_SIGNATUR");
                    return;
            }
            throw new ArgumentOutOfRangeException(writer.Path, value, $"Cannot marshal type SignaturType");
        }

        public static readonly SignaturTypeConverter Singleton = new SignaturTypeConverter();
    }
}