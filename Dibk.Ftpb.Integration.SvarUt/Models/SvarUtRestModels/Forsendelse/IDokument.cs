﻿using System.Collections.Generic;
using System.IO;

namespace Dibk.Ftpb.Integration.SvarUt.Models
{
    public interface IDokument
    {
        string DokumentType { get; set; }
        bool? EkskluderesFraUtskrift { get; set; }
        string Filnavn { get; set; }
        List<long> GiroarkSider { get; set; }
        string MimeType { get; set; }
        bool? SkalSigneres { get; set; }
        Stream DocumentContent { get; set; }
        bool? InneholderPersonsensitivInformasjon { get; set; }
    }
}