﻿using Dibk.Ftpb.Integration.SvarUt.Models.SvarUtRestModels.JsonConverters;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Dibk.Ftpb.Integration.SvarUt.Models
{
    public class ForsendelseStatusResult
    {
        [JsonProperty("statuser")]
        public List<ForsendelseStatus> Statuser { get; set; }

        public static ForsendelseStatusResult FromJson(string json) => JsonConvert.DeserializeObject<ForsendelseStatusResult>(json, SvarUt.Models.Converter.Settings);
    }

    public class ForsendelseStatus
    {
        [JsonProperty("forsendelsesId")]
        public ForsendelsesId ForsendelsesId { get; set; }

        [JsonConverter(typeof(SecondsFrom1970TimeConverter))]
        [JsonProperty("sisteStatusEndring")]
        public DateTime? SisteStatusEndring { get; set; }

        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public Status? Status { get; set; }

        public static ForsendelseStatus FromJson(string json) => JsonConvert.DeserializeObject<ForsendelseStatus>(json, SvarUt.Models.Converter.Settings);
    }

    public static class ForsendelseStatusSerializer
    {
        public static string ToJson(this ForsendelseStatus self) => JsonConvert.SerializeObject(self, SvarUt.Models.Converter.Settings);
    }

    /// <summary>
    /// MOTTATT    Et kall mottatt på forsendelses-service. En id blir tildelt forsendelsen.
    /// AKSEPTERT    Meldingen er validert og forsendelsesfil dannet.
    /// KLAR_FOR_MOTTAK    Venter på at forsendelse skal bli lastet ned av mottaker.
    /// VARSLET    Et varsel om forsendelsen er sendt til varslingstjenesten.
    /// LEST    En forsendelse er lest når hele forsendelsesfilen er lastet ned av mottaker.
    /// SENDT_PRINT    Forsendelsen er blitt overført til printleverandør.
    /// SENDT_DIGITALT    Forsendelsen er motatt og sendt slik den skal. Ikke blitt lest enda.
    /// Forsendelser med denne status vil kun leveres digitalt, og vil aldri gå til print.
    /// SENDT_SDP    Forsendelsen er motatt og sendt til Sikker digital postkasse.
    /// LEVERT_SDP    Forsendelsen er motatt og sendt til Sikker digital postkasse. Vi har motatt
    /// Leveringskvittering fra SDP. Forsendelsen skal da være tilgjengelig for mottaker.
    /// PRINTET    Printkvittering mottatt fra printleverandør eller manuell print bekreftet(via
    /// webgrensesnitt).
    /// AVVIST    Forsendelsen er ikke validert pga. manglende/korrupt metadata, eller fordi
    /// forsendelsesfil ikke kunne dannes.
    /// IKKE_LEVERT    Kun digital leveranse hvor vi ikke har klart å levere forsendelsen.
    /// MANUELT_HANDTERT    Forsendelsen er manuelt avsluttet, f.eks. pga en feilsituasjon.
    /// </summary>
    public enum Status
    { Akseptert, Avvist, IkkeLevert, KlarForMottak, Lest, LevertSdp, ManueltHandtert, Mottatt, Printet, SendtDigitalt, SendtPrint, SendtSdp, Varslet };

    public static class Serialize
    {
        public static string ToJson(this ForsendelseStatusResult self) => JsonConvert.SerializeObject(self, SvarUt.Models.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                StatusConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class StatusConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Status) || t == typeof(Status?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "AKSEPTERT":
                    return Status.Akseptert;

                case "AVVIST":
                    return Status.Avvist;

                case "IKKE_LEVERT":
                    return Status.IkkeLevert;

                case "KLAR_FOR_MOTTAK":
                    return Status.KlarForMottak;

                case "LEST":
                    return Status.Lest;

                case "LEVERT_SDP":
                    return Status.LevertSdp;

                case "MANUELT_HANDTERT":
                    return Status.ManueltHandtert;

                case "MOTTATT":
                    return Status.Mottatt;

                case "PRINTET":
                    return Status.Printet;

                case "SENDT_DIGITALT":
                    return Status.SendtDigitalt;

                case "SENDT_PRINT":
                    return Status.SendtPrint;

                case "SENDT_SDP":
                    return Status.SendtSdp;

                case "VARSLET":
                    return Status.Varslet;
            }
            throw new ArgumentOutOfRangeException(reader.Path, value, $"Cannot unmarshal type Status from value {value}");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Status)untypedValue;
            switch (value)
            {
                case Status.Akseptert:
                    serializer.Serialize(writer, "AKSEPTERT");
                    return;

                case Status.Avvist:
                    serializer.Serialize(writer, "AVVIST");
                    return;

                case Status.IkkeLevert:
                    serializer.Serialize(writer, "IKKE_LEVERT");
                    return;

                case Status.KlarForMottak:
                    serializer.Serialize(writer, "KLAR_FOR_MOTTAK");
                    return;

                case Status.Lest:
                    serializer.Serialize(writer, "LEST");
                    return;

                case Status.LevertSdp:
                    serializer.Serialize(writer, "LEVERT_SDP");
                    return;

                case Status.ManueltHandtert:
                    serializer.Serialize(writer, "MANUELT_HANDTERT");
                    return;

                case Status.Mottatt:
                    serializer.Serialize(writer, "MOTTATT");
                    return;

                case Status.Printet:
                    serializer.Serialize(writer, "PRINTET");
                    return;

                case Status.SendtDigitalt:
                    serializer.Serialize(writer, "SENDT_DIGITALT");
                    return;

                case Status.SendtPrint:
                    serializer.Serialize(writer, "SENDT_PRINT");
                    return;

                case Status.SendtSdp:
                    serializer.Serialize(writer, "SENDT_SDP");
                    return;

                case Status.Varslet:
                    serializer.Serialize(writer, "VARSLET");
                    return;
            }
            throw new ArgumentOutOfRangeException(writer.Path, value, $"Cannot marshal type Status from value {value}");
        }

        public static readonly StatusConverter Singleton = new StatusConverter();
    }
}