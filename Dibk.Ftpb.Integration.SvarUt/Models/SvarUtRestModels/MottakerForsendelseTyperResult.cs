﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.Globalization;

namespace Dibk.Ftpb.Integration.SvarUt.Models
{
    public class MottakerForsendelseTyperResult
    {
        [JsonProperty("treff")]
        public List<MottakerForsendelsesTyper> Treff { get; set; }

        public static MottakerForsendelseTyperResult FromJson(string json) => JsonConvert.DeserializeObject<MottakerForsendelseTyperResult>(json, SvarUt.Models.MottakerForsendelseTyperResultConverter.Settings);
    }

    /// <summary>
    /// En gyldig adresse for levering via SvarInn. Med navn på mottakersystem.
    /// </summary>
    public class MottakerForsendelsesTyper
    {
        [JsonProperty("forsendelsesType")]
        public string ForsendelsesType { get; set; }

        [JsonProperty("mottakerId", NullValueHandling = NullValueHandling.Ignore)]
        public string MottakerId { get; set; }

        [JsonProperty("mottakerSystem")]
        public string MottakerSystem { get; set; }

        [JsonProperty("niva", NullValueHandling = NullValueHandling.Ignore)]
        public long? Niva { get; set; }

        [JsonProperty("organisasjonsNummer", NullValueHandling = NullValueHandling.Ignore)]
        public string OrganisasjonsNummer { get; set; }
    }

    public static class MottakerForsendelseTyperResultSerialize
    {
        public static string ToJson(this MottakerForsendelseTyperResult self) => JsonConvert.SerializeObject(self, SvarUt.Models.MottakerForsendelseTyperResultConverter.Settings);
    }

    internal static class MottakerForsendelseTyperResultConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}