using Dibk.Ftpb.Integration.SvarUt.Builders.Models;

namespace Dibk.Ftpb.Integration.SvarUt.Tests
{
    [TestClass]
    public class ForsendelseDataTests
    {
        private ForsendelseDataBygg GetForsendelseDataBygg()
        {
            return new ForsendelseDataBygg("6b984dd82669")
            {
                Adresselinje1 = "Storgata 1",
                Adresselinje2 = "c/o Ola Nordmann",
                Adresselinje3 = "Postboks 123",
                AvgivendeSystem = "Byggesak",

                G�rdsnummer = "11",
                Bruksnummer = "22",
                Festenummer = "33",
                Seksjonsnummer = "44",
                Bygningsnummer = "1",
                Kommunenummer = "1234",
                ForsendelseType = "UnitTest",
                KommunensSaksnummer�r = "2024",
                KommunensSaksnummerSekvensnummer = "123456789",
                Landkode = "NO",
                Postnr = "1234",
                Poststed = "Oslo",
                S�knadSkjemaNavn = "S�knad om unittest",
                Konteringskode = "UT1234"
            };
        }

        [TestMethod]
        public void ForsendelesteTittel_defaultValue()
        {
            var data = GetForsendelseDataBygg();
            var tittel = "Storgata 1 S�knad om unittest 11/22";

            Assert.AreEqual(data.ForsendelseTittel, tittel);
        }

        [TestMethod]
        public void ForsendelesteTittel_defaultValue_prefix()
        {
            var data = GetForsendelseDataBygg();
            var prefix = "I'm prefix";
            data.ForsendelseTittelPrefix = prefix;

            var tittel = $"{prefix} Storgata 1 S�knad om unittest 11/22";

            Assert.AreEqual(data.ForsendelseTittel, tittel);
        }

        [TestMethod]
        public void ForsendelesteTittel_defaultValue_suffix()
        {
            var data = GetForsendelseDataBygg();
            var suffix = "I'm suffix";
            data.ForsendelseTittelSuffix = suffix;

            var tittel = $"Storgata 1 S�knad om unittest 11/22 {suffix}";

            Assert.AreEqual(data.ForsendelseTittel, tittel);
        }

        [TestMethod]
        public void ForsendelesteTittel_setValue()
        {
            var data = GetForsendelseDataBygg();
            var title = "I'm title";
            data.ForsendelseTittel = title;

            Assert.AreEqual(data.ForsendelseTittel, title);
        }

        [TestMethod]
        public void ForsendelesteTittel_setValue_prefix_suffix()
        {
            var data = GetForsendelseDataBygg();
            var title = "I'm title";
            var prefix = "I'm prefix";
            var suffix = "I'm suffix";
            data.ForsendelseTittel = title;
            data.ForsendelseTittelPrefix = prefix;
            data.ForsendelseTittelSuffix = suffix;

            var tittel = $"{prefix} {title} {suffix}";
            Assert.AreEqual(data.ForsendelseTittel, tittel);
        }
    }
}