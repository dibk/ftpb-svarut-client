﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Dibk.Ftpb.Integration.SvarUt;
using System.IO;
using System;

namespace Dibk.Ftpb.Integration.SvarUtTestApp
{
    class Program
    {
        static void Main(string[] args)
        {

            CreateHostBuilder(args).Build().Run();
        }

        static IHostBuilder CreateHostBuilder(string[] args) =>
          Host.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration((hostingContext, configuration) =>
            {
                configuration.SetBasePath(Directory.GetCurrentDirectory());
                configuration.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                configuration.AddJsonFile("appsettings.local.json", optional: false, reloadOnChange: true);
                configuration.AddEnvironmentVariables();
            })
              .ConfigureServices((_, services) =>
              {

                  services.AddLogging();
                  services.AddSvarUtClient(new Uri(_.Configuration["SvarUtConfigSettings:BaseAddress"]), _.Configuration["SvarUtConfigSettings:UserName"], _.Configuration["SvarUtConfigSettings:Password"]);

                  services.AddHostedService<SvarUtTestWorker>();
              });
    }
}
