﻿using Microsoft.Extensions.Hosting;
using Dibk.Ftpb.Integration.SvarUt;
using Dibk.Ftpb.Integration.SvarUt.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Integration.SvarUtTestApp
{
    public class SvarUtTestWorker : IHostedService
    {
        private readonly ISvarUtClient svarUtClient;

        public SvarUtTestWorker(ISvarUtClient svarUtClient)
        {
            this.svarUtClient = svarUtClient;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("***************************");
            Console.WriteLine("****** Starts worker ******");
            Console.WriteLine("***************************");
            await DoStuff();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine();
            Console.WriteLine("***************************");
            Console.WriteLine("****** Exiting worker *****");
            Console.WriteLine("***************************");
            return Task.FromResult(0);
        }

        public async Task DoStuff()
        {
            /* Get forsendelsestyper */
            Console.WriteLine("------ Get forsendelsestyper ------");
            var forsendelsesTyper = await svarUtClient.GetForsendelsesTyperAsync();
            Console.WriteLine($"Forsendelsestyper ---> {forsendelsesTyper.ToJson()}");
            Console.ReadLine();

            /* Send forsendelse */
            Console.WriteLine("------   Send forsendelse    ------");
            Console.WriteLine();
            ForsendelsesId forsendelsesIdentifikator = null;
            using (var documentStream = new FileStream("mememan_security.png", FileMode.Open))
            {
                var forsendelse = GenerateForsendelse(documentStream);
                forsendelsesIdentifikator = await svarUtClient.SendForsendelseAsync(forsendelse);
            }
            Console.WriteLine();
            Console.WriteLine($"ForsendelsesId ---> {forsendelsesIdentifikator.Id}");
            Console.ReadLine();

            var forsendelsesId = forsendelsesIdentifikator.Id;

            /* Get status */
            Console.WriteLine("------ Get forsendelsesstatus ------");
            Console.WriteLine();
            var status = await svarUtClient.GetForsendelseStatusAsync(forsendelsesId);
            Console.WriteLine();
            Console.WriteLine($"Status ---> {status.ToJson()}");
            Console.WriteLine();
            Console.ReadLine();

            /* Get historikk */
            Console.WriteLine("------ Get forsendelseshistorikk ------");
            Console.WriteLine();
            var historikk = await svarUtClient.GetForsendelseHistorikkAsync(forsendelsesId);
            Console.WriteLine();
            Console.WriteLine($"Historikk ---> {historikk.ToJson()}");
            Console.WriteLine();
            Console.ReadLine();

            /* Get mottaker forsendelsestyper */
            Console.WriteLine("------ Get mottaker forsendelsetyper ------");
            Console.WriteLine();
            var mottakerSystem = await svarUtClient.GetMottakerForsendelseTyperAsync("910065254");
            Console.WriteLine();
            Console.WriteLine($"MottakerSystem ---> {mottakerSystem.ToJson()}");
            Console.WriteLine();
            Console.WriteLine(mottakerSystem.ToJson());
            Console.ReadLine();

            /* Get dokument metadata */
            Console.WriteLine("------ Get dokument metadata ------");
            Console.WriteLine();
            var dokumentMetadata = await svarUtClient.GetDokumentMetadataAsync(forsendelsesId); ;
            Console.WriteLine();
            Console.WriteLine($"Dokumentmetadata ---> {dokumentMetadata?.ToJson()}");
            Console.WriteLine();
            Console.WriteLine(mottakerSystem.ToJson());
            Console.ReadLine();

            /* Get forsendelsestatuser */
            Console.WriteLine("------ Get forsendelsestatuser ------");
            Console.WriteLine();
            var forsendelsesstatuser = await svarUtClient.GetForsendelseStatuserAsync(new List<string> { forsendelsesId });
            Console.WriteLine();
            Console.WriteLine($"Forsendelsestatuser ---> {forsendelsesstatuser?.ToJson()}");
            Console.WriteLine();
            Console.WriteLine(mottakerSystem.ToJson());
            Console.ReadLine();

            /* Get ny forsendelse id */
            Console.WriteLine("------   Get ny forsendelse id    ------");
            Console.WriteLine();
            var nyForsendelsesIdentifikator = await svarUtClient.GetNyForsendelseIdAsync();
            Console.WriteLine();
            Console.WriteLine($"NyForsendelsesId ---> {nyForsendelsesIdentifikator.Id}");
            Console.ReadLine();

            /* Get ny forsendelse id */
            Console.WriteLine("------   Get ny forsendelse id    ------");
            Console.WriteLine();
            ForsendelsesId forsendelse2Id = null;
            using (var documentStream = new FileStream("mememan_security.png", FileMode.Open))
            {
                var forsendelse2 = GenerateForsendelse(documentStream);
                forsendelse2Id = await svarUtClient.SendForsendelseAsync(nyForsendelsesIdentifikator.Id, forsendelse2);
            }
            Console.WriteLine();
            Console.WriteLine($"Opprettet forsendelse ForsendelsesId ---> {forsendelse2Id.Id}");
            Console.ReadLine();

        }

        private Forsendelse GenerateForsendelse(FileStream documentStream)
        {
            var forsendelse = new Forsendelse();

            var randomid = Guid.NewGuid();
            forsendelse.AvgivendeSystem = "SvarUt REST-test Client";
            forsendelse.EksternReferanse = $"REST-TEST {randomid}";
            forsendelse.ForsendelsesType = "Byggesøknad";
            forsendelse.KonteringsKode = "SvarUtTestClientWorker";

            forsendelse.Lenker = new List<Lenke>();
            forsendelse.Lenker.Add(new Lenke() { UrlLenke = "https://www.dibk.no", UrlTekst = "DiBK", LedeTekst = "REST TEST LENKE" });


            forsendelse.Mottaker = new Adresse()
            {
                DigitalAdresse = new Digitaladresse()
                {
                    OrganisasjonsNummer = "910297937"
                },
                PostAdresse = new PostAdresse()
                {
                    Navn = "test_arkitektum",
                    PostNummer = "9999",
                    PostSted = "Digital levering"
                }
            };

            forsendelse.KunDigitalLevering = true;
            forsendelse.Tittel = $"REST-TEST-TITTEL {randomid}";

            forsendelse.Dokumenter = new List<IDokument>();
            var dokument = new SvarUtDokument();
            dokument.DokumentType = "Annet";
            dokument.Filnavn = "mememan_security.png";
            dokument.DocumentContent = documentStream;
            dokument.EkstraMetadata = new List<Entry>() { new Entry() { Key = "TestKey", Value = "TestValue" } };
            forsendelse.Dokumenter.Add(dokument);
            return forsendelse;
        }
    }
}
