﻿using Dibk.Ftpb.Integration.SvarUt;
using Dibk.Ftpb.Integration.SvarUt.Models;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Configuration;
using System.Net.Http;
using System.Text;

namespace Dibk.Ftpb.Integration.SvarUtTestAppLegacy
{
    class Program
    {
        static void Main(string[] args)
        {
            var seriLogger = new LoggerConfiguration().WriteTo.Console().CreateLogger();
            var loggerFactory = (ILoggerFactory)new LoggerFactory();
            loggerFactory.AddSerilog(seriLogger);


            var logger = loggerFactory.CreateLogger<SvarUtClient>();
            var httpClient = new HttpClient()
            {
                BaseAddress = new Uri(ConfigurationManager.AppSettings.Get("SvarUtConfigSettings:BaseAddress")),
                Timeout = TimeSpan.FromMinutes(16)
            };

            var auth = Encoding.ASCII.GetBytes($"{ConfigurationManager.AppSettings.Get("SvarUtConfigSettings:UserName")}:{ConfigurationManager.AppSettings.Get("SvarUtConfigSettings:Password")}");
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(auth));

            var svarUtClient = new SvarUtClient(httpClient, logger);

            logger.LogInformation("Starter kall til SvarUt");
            var forsendelsesTyper = svarUtClient.GetForsendelsesTyperAsync().GetAwaiter().GetResult();
            logger.LogInformation("Svar fra SvarUt mottatt");
            var json = forsendelsesTyper.ToJson();

            Console.WriteLine(json);

            Console.ReadLine();
        }
    }
}
