﻿# SvarUt Forsendelse REST integration
This .Net Standard library integrates with the KS SvarUt REST API.

Nuget package available here: https://www.nuget.org/packages/Dibk.Ftpb.Integration.SvarUt/

## Usage
The `SvarUtClient` is registered by using the extension method in 'SvarUtClientExtensions'. 'SvarUtClient.SendForsendelse' is configured to have a 16 minute timeout. All the other methods use the default timeout value of 100 seconds.


### Dotnet Core
Configuration of services
```csharp
.ConfigureServices((_, services) =>
    {
        services.AddLogging();
        services.AddSvarUtClient(new Uri(_.Configuration["SvarUtConfigSettings:BaseAddress"]), _.Configuration["SvarUtConfigSettings:UserName"], _.Configuration["SvarUtConfigSettings:Password"]);
    });
```

---
### About MimeTypes class
A copy and of the nuget package MimeTypes https://www.nuget.org/packages/MimeTypes which didn't support C# 7.3